#include "AnimatedGameObject.h"


AnimatedGameObject::AnimatedGameObject(double x = 0, double y = 0, double width = 1, double height = 1, double rotation = 0, int maxLife = 1, bool solid = true, std::vector<TextureRegion> regions = std::vector<TextureRegion>(), double duration = 1)
	:  GameObject(x, y, width, height, rotation, new TextureRegion(regions[0].texture, regions[0].x, regions[0].y, regions[0].width, regions[0].height), maxLife, solid)
{
	this->texture = AnimatedTexture(regions, duration);
	time = 0;
}

AnimatedGameObject::~AnimatedGameObject(void)
{
}

AnimatedGameObject::AnimatedGameObject(void)
{
	time = 0;
}

void AnimatedGameObject::update(float delta)
{
	time = delta;
}

void AnimatedGameObject::draw()
{
	// glPushMatrix();
	//glTranslated(playerX, playerY, 0.0);			// Move the player to the required position
	// Bind our player texture to GL_TEXTURE_2D
	
	 textureRegion = this->texture.getKeyframe(time);
	 GameObject::draw();
 //	

	//GLuint t = textureRegion.texture->textureId;
	//glBindTexture(GL_TEXTURE_2D, t);
	////// Enable 2D texturing
	//glEnable(GL_TEXTURE_2D);
	//glEnable(GL_BLEND);
	////// Use two triangles to make a square, with texture co-ordinates for each vertex
	//glBegin(GL_TRIANGLES);
	//	glColor3f(1.0f, 1.0f, 1.0f);
	//	

	//	glTexCoord2f(textureRegion.u1, textureRegion.v1);
	//	glVertex2f(-(this->width / 2), -(this->height / 2));

	//	glTexCoord2f(textureRegion.u2, textureRegion.v1);
	//	glVertex2f((this->width / 2), -(this->height / 2));

	//	glTexCoord2f(textureRegion.u1, textureRegion.v2);
	//	glVertex2f(-(this->width / 2), (this->height / 2));


	//	glTexCoord2f(textureRegion.u2, textureRegion.v1);
	//	glVertex2f((this->width / 2), -(this->height / 2));

	//	glTexCoord2f(textureRegion.u2, textureRegion.v2);
	//	glVertex2f((this->width / 2), (this->height / 2));

	//	glTexCoord2f(textureRegion.u1, textureRegion.v2);
	//	glVertex2f(-(this->width / 2), (this->height / 2));
	//glEnd();

	//// Disable 2D texturing
	//glDisable(GL_TEXTURE_2D);

	//glPopMatrix();

}