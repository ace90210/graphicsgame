#pragma once
#include "DynamicGameObject.h"

class DynamicPlayer: public DynamicGameObject
{
public:

	DynamicPlayer(double x, double y, double width, double height, double rotation, TextureRegion* textureRegion, int maxLife, bool solid, double speed);
	DynamicPlayer(void);
	~DynamicPlayer(void);

	void performCollision(GameObject* otherObject, float delta);
};

