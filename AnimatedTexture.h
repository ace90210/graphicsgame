#pragma once

#include <vector>

#include "TextureRegion.h"


class AnimatedTexture
{
public:
	enum MODE {NONLOOPING, PINGPONG, LOOPING};

	MODE mode;
	std::vector<TextureRegion> keyframes;
	double duration, timeElapsed;
	int n;
	int lastFrame;
	std::vector<int> history;

	AnimatedTexture(void);

	AnimatedTexture(std::vector<TextureRegion> regions, double duration);

	~AnimatedTexture(void);

	TextureRegion* getKeyframe(double time);

};

