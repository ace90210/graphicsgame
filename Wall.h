#pragma once
#include "GameObject.h"

class Wall : public GameObject
{
public:
	static TextureRegion* wallRegion;

	Wall(void);
	Wall(double x, double y, double width, double height, double rotation);
	~Wall(void);

	TextureRegion* createWallRegion();

	void performCollision(GameObject* otherObject, float delta);
};

