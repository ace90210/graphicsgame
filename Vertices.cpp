

#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library

#include "Vertices.h"

Vertices::Vertices(int maxVertices, int maxIndices, bool hasColour, bool hasTexCoords){
	Vertices::hasColour = hasColour;
	Vertices::hasTexCoords = hasTexCoords;
	Vertices::vertexSize = (2 + (hasColour?4:0) + (hasTexCoords?2:0)) * 4;
	Vertices::vertices = new int[maxVertices * vertexSize];

	if(maxIndices > 0)
	{
		Vertices::indices = new short[maxIndices * 16 / 8];
	}
	else
	{
		indices = new short [0];
	}
}

void Vertices::setVertices(float *vertices, int offset, int length){
	Vertices::vertices = new int[ sizeof(vertices) / sizeof(int) ];
	int len = offset + length;
	for(int i = offset, j = 0; i < len; i++, j++){
		Vertices::vertices[j] = vertices[i];
	}
}

void Vertices::setIndices(short *indices, int offset, int length){
	Vertices::indices = new short[ sizeof(indices) / sizeof(short) ];
	int len = offset + length;
	for(int i = offset, j = 0; i < len; i++, j++){
		Vertices::indices[j] = indices[i];
	}
}

void Vertices::bind(){
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_FLOAT, vertexSize, vertices);

	if(Vertices::hasColour)
		{
			glEnableClientState(GL_COLOR_ARRAY);
			glColorPointer(4, GL_FLOAT, vertexSize, vertices);
		}
		if(Vertices::hasTexCoords)
		{
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glTexCoordPointer(2, GL_FLOAT, vertexSize, vertices);
		}
}

void Vertices::unbind(){
	if(Vertices::hasColour)
	{
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}
	if(Vertices::hasTexCoords)
	{
		glDisableClientState(GL_COLOR_ARRAY);
	}
}

void Vertices::draw(int primativeType, int offset, int numVertices){
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_FLOAT, vertexSize, vertices);
		
	if(Vertices::hasColour)
	{
		glEnableClientState(GL_COLOR_ARRAY);
		glColorPointer(4, GL_FLOAT, vertexSize, vertices);
	}
	if(Vertices::hasTexCoords)
	{
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, vertexSize, vertices);
	}
		
	if(sizeof(Vertices::indices) > 0)
	{
		glDrawElements(primativeType, numVertices,  GL_UNSIGNED_SHORT,  indices);
	}
	else
	{
		glDrawArrays(primativeType, offset, numVertices);
	}
		
	if(Vertices::hasTexCoords)
	{
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}
		
	if(Vertices::hasColour)
	{
		glDisableClientState(GL_COLOR_ARRAY);
	}
}

void Vertices::drawOptimal(int primativeType, int offset, int numVertices)
	{				
		if(sizeof(indices) > 0)
		{
			glDrawElements(primativeType, numVertices,  GL_UNSIGNED_SHORT,  indices);
		}
		else
		{
			glDrawArrays(primativeType, offset, numVertices);
		}
	}