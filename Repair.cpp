#include "Repair.h"

TextureRegion* Repair::repairRegion = NULL;

Repair::Repair(void)
{
}


Repair::~Repair(void)
{
}



Repair::Repair (int repairValue, double x, double y, double width, double height, double rotation)
	: GameObject( x, y, width, height, rotation, createRepairRegion(), 1, true)
{
	collisionId = REPAIR; 
	collidesWith = TANK;
	this->repairValue = repairValue;
}


TextureRegion* Repair::createRepairRegion()
{
	if(repairRegion == NULL)
	{
		repairRegion = new TextureRegion("repair.png");
	}
	return repairRegion;
}


void Repair::performCollision(GameObject* otherObject, float delta)
{
	if(otherObject->collisionId == TANK && otherObject->currentLife < otherObject->maxLife)
	{
		otherObject->currentLife += repairValue;
		if(otherObject->currentLife > otherObject->maxLife)
		{
			otherObject->currentLife = otherObject->maxLife;
		}
		currentLife--;
	}

	if(currentLife < 1)
	{
		solid = false;
	}
}