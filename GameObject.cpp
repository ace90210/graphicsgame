
#include "GameObject.h"

DEBUG_MODE GameObject::debug = OFF;

GameObject::GameObject(double x = 0, double y = 0, double width = 1, double height= 1, double rotation = 0, TextureRegion* textureRegion = new TextureRegion(), int maxLife = 1, bool solid = true)
{
	position = Vector2(x, y);
	this->width = width;
	this->height = height;
	this->maxLife = maxLife;
	this->currentLife = maxLife;
	this->solid = solid;
	this->oRotation = rotation;
	this->speed = 0;
	this->maxSpeed = 1;
	this->invincible = false;

	topLeft = Vector2(-(width / 2), -(height / 2));
	topRight = Vector2((width / 2), -(height / 2));
	bottomLeft = Vector2(-(width / 2), (height / 2));
	bottomRight = Vector2((width / 2), (height / 2));
	
	

	this->boundingBox = Shape(4);
	/*this->boundingBox.vertices[0] = Vector2(topLeft.x + x, topLeft.y + y);
	this->boundingBox.vertices[1] = Vector2(topRight.x + x, topRight.y + y);
	this->boundingBox.vertices[2] = Vector2(bottomRight.x + x, bottomRight.y + y);
	this->boundingBox.vertices[3] = Vector2(bottomLeft.x + x, bottomLeft.y + y);*/
	this->boundingBox.vertices[0] = Vector2(topLeft.x, topLeft.y);
	this->boundingBox.vertices[1] = Vector2(topRight.x, topRight.y);
	this->boundingBox.vertices[2] = Vector2(bottomRight.x, bottomRight.y);
	this->boundingBox.vertices[3] = Vector2(bottomLeft.x, bottomLeft.y);
	templateBoundingBox = boundingBox;
	

	//for debugging
	t0 = this->boundingBox.vertices[0];
	t1 = this->boundingBox.vertices[1];
	t2 = this->boundingBox.vertices[2];
	t3 = this->boundingBox.vertices[3];
	
	
	topLeft.rotate(oRotation);
	topRight.rotate(oRotation);
	bottomLeft.rotate(oRotation);
	bottomRight.rotate(oRotation);

	this->boundingBox.vertices[0].rotate(oRotation);
	this->boundingBox.vertices[1].rotate(oRotation);
	this->boundingBox.vertices[2].rotate(oRotation);
	this->boundingBox.vertices[3].rotate(oRotation);
	
	//move back to location on map
	this->boundingBox.vertices[0].add(this->getPosition());
	this->boundingBox.vertices[1].add(this->getPosition());
	this->boundingBox.vertices[2].add(this->getPosition());
	this->boundingBox.vertices[3].add(this->getPosition());

	//for debugging
	t0 = this->boundingBox.vertices[0];
	t1 = this->boundingBox.vertices[1];
	t2 = this->boundingBox.vertices[2];
	t3 = this->boundingBox.vertices[3];


	this->textureRegion = textureRegion;

	//if solid collide with everything by default, else collide with nothing
	if(solid)
	{
		collidesWith = ~0; //Not 0 == all 1's in binary aka maz int value, therefor by default collides with everything
	}
	else
	{
		collidesWith = 0; // collides with nothing
	}
	ignoreCollisionsWith = 0; // default ignore nothing
	collisionId = DEFAULT; //collision id used for collision flags set to default of 1
}

GameObject::GameObject(void)
{
}

GameObject::~GameObject(void)
{
}

void GameObject::draw()
{
	
	//only draw things that are alive or invincible (aka alive regardless of life)
	if(currentLife >0 || invincible ){
		glPushMatrix();
		glTranslated(position.x, position.y, 0.0);			// Move the player to the required position
		// Bind our player texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, textureRegion->texture->textureId);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Use two triangles to make a square, with texture co-ordinates for each vertex
		glBegin(GL_TRIANGLES);
			glColor3f(1.0f, 1.0f, 1.0f);

			glTexCoord2f(textureRegion->u1, textureRegion->v1);
			glVertex2f(topLeft.x, topLeft.y);

			glTexCoord2f(textureRegion->u2, textureRegion->v1);
			glVertex2f(topRight.x, topRight.y);

			glTexCoord2f(textureRegion->u1, textureRegion->v2);
			glVertex2f(bottomLeft.x, bottomLeft.y);


			glTexCoord2f(textureRegion->u2, textureRegion->v1);
			glVertex2f(topRight.x, topRight.y);

			glTexCoord2f(textureRegion->u2, textureRegion->v2);
			glVertex2f(bottomRight.x, bottomRight.y);

			glTexCoord2f(textureRegion->u1, textureRegion->v2);
			glVertex2f(bottomLeft.x, bottomLeft.y);
		glEnd();

		// Disable 2D texturing
		glDisable(GL_TEXTURE_2D);

		glPopMatrix();


		//draw debug bounding box
		if(debug == ALL || (debug == SOLIDS_ONLY && solid))
		{
			glPushMatrix();
				glDisable(GL_TEXTURE_2D);
				glDisable(GL_BLEND);
				//for debugging
				Vector2 t0 = this->boundingBox.vertices[0];
				Vector2 t1 = this->boundingBox.vertices[1];
				Vector2 t2 = this->boundingBox.vertices[2];
				Vector2 t3 = this->boundingBox.vertices[3];
				glBegin(GL_LINES);
				if(solid)
				{
					glColor4f(1, 0, 0, 0);//solids red non solids blue
				}
				else
				{
					glColor4f(0, 0, 1, 0);
				}
					glVertex2d(this->boundingBox.vertices[0].x, this->boundingBox.vertices[0].y);        //top left to     
					glVertex2d( this->boundingBox.vertices[1].x, this->boundingBox.vertices[1].y);       //top right    

					glVertex2d( this->boundingBox.vertices[1].x, this->boundingBox.vertices[1].y);       //top right to  
					glVertex2d( this->boundingBox.vertices[2].x, this->boundingBox.vertices[2].y);       //bottom left

					glVertex2d( this->boundingBox.vertices[2].x, this->boundingBox.vertices[2].y);       //bottom left to
					glVertex2d(this->boundingBox.vertices[3].x, this->boundingBox.vertices[3].y);		//bottom right

					glVertex2d(this->boundingBox.vertices[3].x, this->boundingBox.vertices[3].y);		//bottom right to
					glVertex2d(this->boundingBox.vertices[0].x, this->boundingBox.vertices[0].y);       //top left      

					glColor4f(1, 1, 1, 1);
				glEnd();
				
			glPopMatrix();		

		}
	}
}

bool GameObject::isAlive()
{
	return currentLife > 0;
}

void GameObject::rotate(double angle)
{
	oRotation += angle;	
	topLeft.rotate(angle);
	topRight.rotate(angle);
	bottomLeft.rotate(angle);
	bottomRight.rotate(angle);

	/*this->boundingBox.vertices[0] = Vector2(topLeft.x + this->getPosition().x, topLeft.y + this->getPosition().y);
	this->boundingBox.vertices[1] = Vector2(topRight.x + this->getPosition().x, topRight.y + this->getPosition().y);
	this->boundingBox.vertices[2] = Vector2(bottomRight.x + this->getPosition().x, bottomRight.y + this->getPosition().y);
	this->boundingBox.vertices[3] = Vector2(bottomLeft.x + this->getPosition().x, bottomLeft.y + this->getPosition().y);*/

	//set back to origon before rotation
	this->boundingBox.vertices[0].subtract(this->getPosition());
	this->boundingBox.vertices[1].subtract(this->getPosition());
	this->boundingBox.vertices[2].subtract(this->getPosition());
	this->boundingBox.vertices[3].subtract(this->getPosition());

	this->boundingBox.vertices[0].rotate(angle);
	this->boundingBox.vertices[1].rotate(angle);
	this->boundingBox.vertices[2].rotate(angle);
	this->boundingBox.vertices[3].rotate(angle);

	//move back to location on map
	this->boundingBox.vertices[0].add(this->getPosition());
	this->boundingBox.vertices[1].add(this->getPosition());
	this->boundingBox.vertices[2].add(this->getPosition());
	this->boundingBox.vertices[3].add(this->getPosition());

	//for debugging
	t0 = this->boundingBox.vertices[0];
	t1 = this->boundingBox.vertices[1];
	t2 = this->boundingBox.vertices[2];
	t3 = this->boundingBox.vertices[3];
}

void GameObject::setRotation(double angle)
{
	this->rotate(-oRotation);
	this->rotate(angle);
	
	//oRotation = angle;


	////set to 0 degrees
	//topLeft.rotate(-oRotation);
	//topRight.rotate(-oRotation);
	//bottomLeft.rotate(-oRotation);
	//bottomRight.rotate(-oRotation);

	///*this->boundingBox.vertices[0] = Vector2(topLeft.x + this->getPosition().x, topLeft.y + this->getPosition().y);
	//this->boundingBox.vertices[1] = Vector2(topRight.x + this->getPosition().x, topRight.y + this->getPosition().y);
	//this->boundingBox.vertices[2] = Vector2(bottomRight.x + this->getPosition().x, bottomRight.y + this->getPosition().y);
	//this->boundingBox.vertices[3] = Vector2(bottomLeft.x + this->getPosition().x, bottomLeft.y + this->getPosition().y);*/

	////set back to origon before rotation
	//this->boundingBox.vertices[0].subtract(this->getPosition());
	//this->boundingBox.vertices[1].subtract(this->getPosition());
	//this->boundingBox.vertices[2].subtract(this->getPosition());
	//this->boundingBox.vertices[3].subtract(this->getPosition());

	//this->boundingBox.vertices[0].rotate(angle);
	//this->boundingBox.vertices[1].rotate(angle);
	//this->boundingBox.vertices[2].rotate(angle);
	//this->boundingBox.vertices[3].rotate(angle);

	////move back to location on map
	//this->boundingBox.vertices[0].add(this->getPosition());
	//this->boundingBox.vertices[1].add(this->getPosition());
	//this->boundingBox.vertices[2].add(this->getPosition());
	//this->boundingBox.vertices[3].add(this->getPosition());

	////for debugging
	//t0 = this->boundingBox.vertices[0];
	//t1 = this->boundingBox.vertices[1];
	//t2 = this->boundingBox.vertices[2];
	//t3 = this->boundingBox.vertices[3];
	/*

	this->boundingBox.vertices[0] = templateBoundingBox.vertices[0];
	this->boundingBox.vertices[1] = templateBoundingBox.vertices[1];
	this->boundingBox.vertices[2] = templateBoundingBox.vertices[2];
	this->boundingBox.vertices[3] = templateBoundingBox.vertices[3];*/

	
	
	//
	//topLeft.rotate(oRotation);
	//topRight.rotate(oRotation);
	//bottomLeft.rotate(oRotation);
	//bottomRight.rotate(oRotation);

	//this->boundingBox.vertices[0].rotate(angle);
	//this->boundingBox.vertices[1].rotate(angle);
	//this->boundingBox.vertices[2].rotate(angle);
	//this->boundingBox.vertices[3].rotate(angle);

	////move to location on map
	//this->boundingBox.vertices[0].add(this->getPosition());
	//this->boundingBox.vertices[1].add(this->getPosition());
	//this->boundingBox.vertices[2].add(this->getPosition());
	//this->boundingBox.vertices[3].add(this->getPosition());

	////for debugging
	//t0 = this->boundingBox.vertices[0];
	//t1 = this->boundingBox.vertices[1];
	//t2 = this->boundingBox.vertices[2];
	//t3 = this->boundingBox.vertices[3];
}

double GameObject::getRotation()
{
	double normalisedRotation =  (((int)GameObject::oRotation + 180) % 360);
	return normalisedRotation - 180;
}

void GameObject::setPosition(double x, double y)
{
	this->addPosition(-position.x, -position.y);

	this->addPosition(x, y);

	////move to origin
	//this->boundingBox.vertices[0].subtract(this->getPosition());
	//this->boundingBox.vertices[1].subtract(this->getPosition());
	//this->boundingBox.vertices[2].subtract(this->getPosition());
	//this->boundingBox.vertices[3].subtract(this->getPosition());
	//
	////update position
	//position.x = x;
	//position.y = y;

	////move to location on map
	//this->boundingBox.vertices[0].add(this->getPosition());
	//this->boundingBox.vertices[1].add(this->getPosition());
	//this->boundingBox.vertices[2].add(this->getPosition());
	//this->boundingBox.vertices[3].add(this->getPosition());
}

void GameObject::addPosition(double x, double y)
{
	position.x += x;
	position.y += y;

	this->boundingBox.vertices[0].x += x;
	this->boundingBox.vertices[0].y += y;
	this->boundingBox.vertices[1].x += x;
	this->boundingBox.vertices[1].y += y;
	this->boundingBox.vertices[2].x += x;
	this->boundingBox.vertices[2].y += y;
	this->boundingBox.vertices[3].x += x;
	this->boundingBox.vertices[3].y += y;
}
	
Vector2 GameObject::getPosition()
{
	return this->position;
}

void GameObject::setBoundingBox(double x, double y, double width, double height)
{
	//convert co-ords to unit verions then scale by unit size
	double unitX = x / this->textureRegion->width;
	double unitY = y / this->textureRegion->height;
	double unitW = width / this->textureRegion->width;
	double unitH = height / this->textureRegion->height;
	x = unitX * this->width;
	y = unitY * this->height;
	width = this->width * unitW;
	height = this->height * unitH;

	this->templateBoundingBox.vertices[0] = Vector2( x        , this->height - y - height );
	this->templateBoundingBox.vertices[1] = Vector2( x + width, this->height - y - height );
	this->templateBoundingBox.vertices[2] = Vector2( x + width, this->height - y );
	this->templateBoundingBox.vertices[3] = Vector2( x        , this->height - y );

	//offset by half width (set centre to origin
	this->templateBoundingBox.vertices[0].subtract(this->width / 2, this->height / 2);
	this->templateBoundingBox.vertices[1].subtract(this->width / 2, this->height / 2);
	this->templateBoundingBox.vertices[2].subtract(this->width / 2, this->height / 2);
	this->templateBoundingBox.vertices[3].subtract(this->width / 2, this->height / 2);

	this->boundingBox.vertices[0].rotate(oRotation);
	this->boundingBox.vertices[1].rotate(oRotation);
	this->boundingBox.vertices[2].rotate(oRotation);
	this->boundingBox.vertices[3].rotate(oRotation);
	
	//move back to location on map
	this->boundingBox.vertices[0].add(this->getPosition());
	this->boundingBox.vertices[1].add(this->getPosition());
	this->boundingBox.vertices[2].add(this->getPosition());
	this->boundingBox.vertices[3].add(this->getPosition());

	
	t0 = this->boundingBox.vertices[0];
	t1 = this->boundingBox.vertices[1];
	t2 = this->boundingBox.vertices[2];
	t3 = this->boundingBox.vertices[3];
}




void GameObject::cycleDebugModes()
{
	GameObject::debug = (DEBUG_MODE)((debug + 1) % 3);
}

std::string GameObject::getDebugName()
{
	switch(debug)
	{
		case OFF: return "OFF";
		case SOLIDS_ONLY: return "SOLIDS ONLY";
		case ALL: return "ALL";
		default: return "Unknown";
	}
}