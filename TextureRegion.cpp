#include "TextureRegion.h"

TextureRegion::TextureRegion(void)
{

}

TextureRegion::TextureRegion(std::string textureURL)
{
	texture = new Texture(textureURL);
	TextureRegion::x = 0;
	TextureRegion::y = 0;
	TextureRegion::width = texture->width;
	TextureRegion::height = texture->height;

	TextureRegion::u1 = 0;
	TextureRegion::v1 = 0;

	TextureRegion::u2 = 1;
	TextureRegion::v2 = 1;
}


TextureRegion::TextureRegion(std::string textureIn, double x, double y, double width, double height)
{
	texture = new Texture(textureIn);
	
	TextureRegion::x = x;
	
	double newY = (texture->height - y - height);
	TextureRegion::y = newY;

	//TextureRegion::y = y;
	TextureRegion::width = width;
	TextureRegion::height = height;

	TextureRegion::u1 = x / texture->width;
	TextureRegion::v1 = TextureRegion::y / texture->height;

	TextureRegion::u2 = TextureRegion::u1 + (width / texture->width);
	TextureRegion::v2 = TextureRegion::v1 + (height / texture->height);
}

TextureRegion::TextureRegion(Texture* origTexture, double x, double y, double width, double height)
{
	TextureRegion::texture = origTexture;

	TextureRegion::x = x;
	TextureRegion::y = (texture->height - y - height);
	TextureRegion::width = width;
	TextureRegion::height = height;

	TextureRegion::u1 = x / texture->width;
	TextureRegion::v1 = TextureRegion::y / texture->height;

	TextureRegion::u2 = TextureRegion::u1 + (width / texture->width);
	TextureRegion::v2 = TextureRegion::v1 + (height / texture->height);
}

TextureRegion::TextureRegion(Texture* origTexture, double texWidth, double texHeight, double x, double y, double width, double height)
{
	TextureRegion::texture = origTexture;
	TextureRegion::x = x;
	TextureRegion::y = (texture->height - y - height);
	TextureRegion::width = width;
	TextureRegion::height = height;

	TextureRegion::u1 = x / texWidth;
	TextureRegion::v1 = TextureRegion::y / texHeight;

	TextureRegion::u2 = TextureRegion::u1 + (width / texWidth);
	TextureRegion::v2 = TextureRegion::v1 + (height / texHeight);
}

