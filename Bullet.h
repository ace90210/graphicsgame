#pragma once

#include "DynamicGameObject.h"
#include "CollisionGrid.h"
#include "Nuke.h"

class Bullet: public DynamicGameObject
{
public:
static const int LIFE_LIMIT = 10;  // seconds
	long double timeAlive, distanceCovered, distanceLimit;

	Vector2 startPosition;
	vector<Bullet*>* firedBullets;
	CollisionGrid* grid;	
	static TextureRegion* bulletRegion;

	Nuke* explosion;

	bool alive;

	Bullet(double x, double y, double width, double height, double rotation, int maxLife, double speed, double distance, vector<Bullet*>* firedBullets, CollisionGrid* grid);
	Bullet(void);
	~Bullet(void);


	TextureRegion* createBulletRegion();

	void deleteBullet();

	void update(double delta);
	void performCollision(GameObject* otherObject, float delta);

	void draw();
};

