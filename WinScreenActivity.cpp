//
// The structure of the Graphics 1 OpenGL template is explained in README.txt
//

#include <windows.h>		// Header File For Windows
#include <gl/gl.h>			// Header File For The OpenGL32 Library
#include <gl/glu.h>			// Header File For The GLu32 Library

#include "SOIL.h"

#include "OpenGLApplication.h"			// Needed for OpenGLApplication method calls
#include "WinScreenActivity.h"
#include "GameActivity.h"


#include "freetype.h"		// Header for font library.
using namespace freetype;


font_data win_font;

WinScreenActivity::WinScreenActivity(OpenGLApplication *app)
	: Activity(app)
{
	
	win_font.init("arialbd.TTF", 36);					    //Build the freetype font
}


void WinScreenActivity::initialise()
{
	// Initialise the activity; called at application start up

	// Load the start screen image as a texture using the SOIL library
	textureID = SOIL_load_OGL_texture("winscreen.png",			// filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);				// generate Mipmaps and invert Y
}


void WinScreenActivity::shutdown()
{
	// Shutdown the activity; called at application finish

	// Delete the texture
	glDeleteTextures(1, &textureID);
}


/*
*
* ACTIVITY METHODS
* Put your application/game code here
*
*/
void WinScreenActivity::onSwitchIn()
{
	// Activity switched in

		glClearColor(0.37, 0.49, 0.32, 0.0);						//sets the clear colour to same as end image background
}

void WinScreenActivity::onReshape(int width, int height)
{
	// Screen resized
	glViewport(0,0,width,height);						// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	double aspect = app->getAspectRatio();
	gluOrtho2D(-aspect, aspect, -1.0, 1.0);				// set the coordinate system for the window

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
}

void WinScreenActivity::render()
{
	// OpenGL render calls go in this method

	// Clear color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	// Identity matrix
	glLoadIdentity();

	// Bind our start screen texture to GL_TEXTURE_2D
	glBindTexture(GL_TEXTURE_2D, textureID);
	// Enable 2D texturing
	glEnable(GL_TEXTURE_2D);

	// Use two triangles to make a square, with texture co-ordinates for each vertex
	glBegin(GL_TRIANGLES);
		glTexCoord2f(0, 0);
		glVertex2f(-1, -1);

		glTexCoord2f(1, 0);
		glVertex2f(1, -1);

		glTexCoord2f(0, 1);
		glVertex2f(-1, 1);


		glTexCoord2f(1, 0);
		glVertex2f(1, -1);

		glTexCoord2f(1, 1);
		glVertex2f(1, 1);

		glTexCoord2f(0, 1);
		glVertex2f(-1, 1);
	glEnd();

	// Disable 2D texturing
	glDisable(GL_TEXTURE_2D);

	double scale = (this->app->getScreenHeight() / VIEW_SIZE) / BASE_SCALE;

	glLoadIdentity();	
	print(win_font, scale * 2, 0.40f,  0.95f, 0.40f, this->app->getScreenWidth() / 3, this->app->getScreenHeight() - (75 * scale), "You WIN!");
	print(win_font, scale, 0.40f,  0.95f, 0.40f,  this->app->getScreenWidth() / 3,  (75 * scale) , "You Scored %d", GameActivity::score);

	glFlush();
}



void WinScreenActivity::onKeyUp(int key)										// Called when key released
{
	// Key released

	// Exit the start screen when the SPACE key is released, NOT pressed
	// That way the next activity starts with the space key NOT pressed
	if (key == ' ')
	{
		// Space; finish the application
		app->finish();
	}
}
