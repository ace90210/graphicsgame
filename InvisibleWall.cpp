#include "InvisibleWall.h"

TextureRegion* InvisibleWall::wallRegion = NULL;

InvisibleWall::InvisibleWall(void)
{
}


InvisibleWall::~InvisibleWall(void)
{
}

InvisibleWall::InvisibleWall(Vector2 start, double width, double height, double rotation)
	:  GameObject(start.x, start.y, width, height, rotation, createWallRegion(), 1, true)
{
	invincible = true;
	ignoreCollisionsWith = EXPLOSION;
}

TextureRegion* InvisibleWall::createWallRegion()
{
	if(wallRegion == NULL)
	{
		wallRegion = new TextureRegion("invisible.png");
	}
	return wallRegion;
}

void InvisibleWall::performCollision(GameObject* otherObject, float delta)
{
	
}