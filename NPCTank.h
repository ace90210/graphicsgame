#pragma once
#include "Tank.h"
#include "CollisionGrid.h"

class NPCTank: public Tank
{
	Tank* player;
	long double randomDelay, targetPlayerDirection, targetPlayerDistance, reversingTime, forwardTime;
	bool doAvoidanceProcedure;
	double avoidingTurnProgress;
	bool altTurnDirection, seenPlayer;

public:
	NPCTank(void);
	NPCTank(double x, double y,  double rotation, int health, double speed, double fireRate,  CollisionGrid* grid, Tank* player);
	~NPCTank(void);

	double genRandDouble(double fMin, double fMax);


	bool tankNeedsRotating(double angleDifference);


	virtual void update(double delta);
	
	void performCollision(GameObject* otherObject, float delta);
};

