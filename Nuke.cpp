#include "Nuke.h"

#define GLOBAL_VOLUME 0.5

Texture* Nuke::atlas = NULL;
SoundEffect* Nuke::explosion = NULL;
std::vector<TextureRegion>* Nuke::regions = NULL;


Nuke::Nuke(void)
{
}


Nuke::~Nuke(void)
{
}

Nuke::Nuke(double x, double y, double width, double height, double duration)
	:  AnimatedGameObject(x, y, width, height, 0, 1, true, createRegions(), duration)
{
	collisionId = EXPLOSION;
	ignoreCollisionsWith = EXPLOSION | BULLET | WALL ;
	collidesWith = TANK | TRAP | MINE;
	explosionLife = frameNum * duration;
	if(explosion == NULL)
	{
		explosion = new SoundEffect("explosion.mp3");	
	}
	loaded = true;
	play = false;
	done = false;
	this->texture.mode = AnimatedTexture::MODE::LOOPING;
	explosion->setGlobalVolume(GLOBAL_VOLUME);
}

std::vector<TextureRegion> Nuke::createRegions()
{
	
	frameNum = 0;
	if(atlas == NULL)
	{
		atlas = new Texture("explo2.png");
	}
	if(regions == NULL)
	{
		regions = new std::vector<TextureRegion>();
		regions->push_back(TextureRegion(atlas, 0, 0, 63, 63));
		regions->push_back(TextureRegion(atlas, 64, 0, 63, 63));
		regions->push_back(TextureRegion(atlas, 128, 0, 63, 63));
		regions->push_back(TextureRegion(atlas, 192, 0, 63, 63));

		regions->push_back(TextureRegion(atlas, 0, 64, 63, 63));
		regions->push_back(TextureRegion(atlas, 64, 64, 63, 63));
		regions->push_back(TextureRegion(atlas, 128, 64, 63, 63));
		regions->push_back(TextureRegion(atlas, 192, 64, 63, 63));

		regions->push_back(TextureRegion(atlas, 0, 128, 63, 63));
		regions->push_back(TextureRegion(atlas, 64, 128, 63, 63));
		regions->push_back(TextureRegion(atlas, 128, 128, 63, 63));
		regions->push_back(TextureRegion(atlas, 192, 128, 63, 63));

		regions->push_back(TextureRegion(atlas, 0, 192, 63, 63));
		regions->push_back(TextureRegion(atlas, 64, 192, 63, 63));
		regions->push_back(TextureRegion(atlas, 128, 192, 63, 63));
		regions->push_back(TextureRegion(atlas, 192, 192, 63, 63));
	}
	frameNum = regions->size();
	return *regions;
}

void Nuke::update(double delta)
{
	
	if(this->isAlive() && loaded && play)
	{
		if(!explosion->isPlaying() && play)
		{
			explosion->Play();
		}
		AnimatedGameObject::update(delta);
		if(texture.timeElapsed > explosionLife)
		{
			currentLife = 0;
			done = true;
			solid = false;
		}
	}
}

void Nuke::performCollision(GameObject* otherObject, float delta)
{
	/*if(otherObject->collisionId == TANK)
	{
		otherObject->currentLife--;
	}*/
	if(!otherObject->invincible)
	{
		otherObject->currentLife--;
		solid = false;
	}
}

void Nuke::startPlayback(float volume)
{
	play = true;
	this->time = 0;
	texture.timeElapsed = 0;
	if(explosion->isPlaying())
	{
		explosion->Stop();
	}
	currentLife = 1;
	loaded = true;
	done = false;
	this->explosion->SetVolume(volume);
}

void Nuke::unload()
{
	//explosion->Unload();
	loaded = false;
}