#pragma once

class Vertices {
private:
	bool hasColour, hasTexCoords;
	int vertexSize;
	int *vertices;
	short *indices;
	int *tmpBuffer;

public:
	Vertices(int maxVertices, int maxIndices, bool hasColour, bool hasTexCoords);

	void setVertices(float *vertices, int offset, int length);

	void setIndices(short *indices, int offset, int length);

	void bind();

	void unbind();

	void draw(int primativeType, int offset, int numVertices);

	void drawOptimal(int primativeType, int offset, int numVertices);
};