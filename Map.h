#pragma once
#include <vector>
#include "TileEntity.h"

using namespace std;

class Map
{
public:
	int backgroundId;

	vector<vector<TileEntity>> tiles;

	Map(int backgroundId, vector<vector<TileEntity>> tiles);
	Map(void);
	~Map(void);
};

