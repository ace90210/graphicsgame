#pragma once
#include <vector>
#include "Turret.h"
#include "Bullet.h"

#include "CollisionGrid.h"

#include "FMODsound.h"

#include "freetype.h"		// Header for font library.

using namespace freetype;

using namespace std;

class Tank: public DynamicGameObject
{
protected:
	SoundEffect fire_sound;
	SoundEffect drive_sound;
	SoundEffect crash_sound;
	//SoundEffect turret_sound; //too much trouble temp disabled

	CollisionGrid* grid;

	Turret gunTurret;
	vector<Bullet*> firedBullets;

	double timeSinceFired, fireRate, targetTurretDirection;
	bool driveSoundEnabled, playTurretTurnSound;

	//used in collisions for optimizasion
	double directionToObject;
	double distanceToCamera;
	double lastMovedDistance;
public:
	static font_data hp_font;

	static TextureRegion* turretRegion;
	static TextureRegion* baseRegion;

	Tank(double x, double y, double width, double height, double rotation,  int maxLife, bool solid, double speed, CollisionGrid* grid);
	Tank(void);
	~Tank(void);

	TextureRegion* createBaseRegion();

	void rotateTurret(double rotation);
	void setTurretRotation(double rotation);

	bool fire();
	bool fire(double distance);

	bool turretNeedsRotating();
	bool turretNeedsRotating(double angleDifference);

	void drawHealthNumbers();

	void drawHealthBar();

	void drawBullets();

	void playTurretSound();

	void stopTurretSound();

	void stopAllSounds();

	//overriden from GameObject
	virtual void draw();	
	virtual void rotate(double angle);
	virtual void setRotation(double angle);
	void performCollision(GameObject* otherObject, float delta);

	//Overriden from DynamicGameObject
	virtual void update(double delta);
	virtual void stop();

};

