#include "SaveFile.h"
#include <iostream>
#include <fstream>
#include <string>

#include <sstream>

SaveFile::SaveFile(void)
{
}

SaveFile::~SaveFile(void)
{
}

void SaveFile::saveMap(vector<vector<int>> map)
{
	string filename = "test.tankz";
	fstream file_op(filename.c_str(), ios::in);
	fstream binary_file(filename.c_str(), ios::out|ios::binary);
	int mapSizeW = map.size();
	int mapSizeH = map[0].size();

	binary_file.write(reinterpret_cast<char *>(&mapSizeW),sizeof(mapSizeW));	//write map width
	binary_file.write(reinterpret_cast<char *>(&mapSizeH),sizeof(mapSizeH));	//write map height

	for(int x = 0; x < map.size(); x++)
	{
		for(int y = 0; y < map[y].size(); y++)
		{
			binary_file.write(reinterpret_cast<char *>(&map[x][y]),sizeof(map[x][y]));	//write tile value
		}
	}

	binary_file.close();
}

vector<vector<int>> SaveFile::loadMap()
{
	string filename = "test.tankz";
	fstream file_op(filename.c_str(), ios::in);
	vector<vector<int>> output = vector<vector<int>>();

	if(file_op.is_open())
	{
		fstream binary_file(filename.c_str(), ios::in|ios::binary);
		int mapSizeW = 0;
		int mapSizeH = 0;
		binary_file.read(reinterpret_cast<char *>(&mapSizeW),sizeof(mapSizeW)); //read map width
		binary_file.read(reinterpret_cast<char *>(&mapSizeH),sizeof(mapSizeH)); //read map height

		for(int x = 0; x < mapSizeW; x++)
		{
			vector<int> row;
			for(int y = 0; y < mapSizeH; y++)
			{
				int value = 0;
				binary_file.read(reinterpret_cast<char *>(&value),sizeof(value)); //read tile value
				row.push_back(value);
			}
			output.push_back(row);
		}
	}
	return output;
}

void SaveFile::saveMapS(Map map)
{
	string filename = "testS.tankz";

	saveMap(filename,  map);
}

Map SaveFile::loadMapS()
{
	string filename = "testS.tankz";

	return SaveFile::loadMap(filename);
}


void SaveFile::saveMapLevel(int level, Map map)
{
	ostringstream defaultname;
	defaultname << "levels\\level_" << level << ".tankz";	
	string filename = defaultname.str();

	saveMap(filename,  map);
}

Map SaveFile::loadMapLevel(int level)
{
	ostringstream defaultname;
	defaultname << "levels\\level_" << level << ".tankz";	
	string filename = defaultname.str();

	
	return SaveFile::loadMap(filename);
}

void SaveFile::saveMapLevel(int level, int wave, Map map)
{
	ostringstream defaultname;
	defaultname << "levels\\level_" << level << "_" << wave << ".tankz";	
	string filename = defaultname.str();

	saveMap(filename,  map);
}


Map SaveFile::loadMapLevel(int level, int wave){
	ostringstream defaultname;
	defaultname << "levels\\level_" << level << "_" << wave << ".tankz";	
	string filename = defaultname.str();

	
	return SaveFile::loadMap(filename);
}


void SaveFile::saveMap(string filename, Map map)
{
	vector<vector<TileEntity>> tiles = map.tiles;

	fstream file_op(filename.c_str(), ios::in);
	fstream binary_file(filename.c_str(), ios::out|ios::binary);
	int background = map.backgroundId;
	int mapSizeW = tiles.size();
	int mapSizeH = tiles[0].size();

	binary_file.write(reinterpret_cast<char *>(&background),sizeof(background));	//write map width
	binary_file.write(reinterpret_cast<char *>(&mapSizeW),sizeof(mapSizeW));	//write map width
	binary_file.write(reinterpret_cast<char *>(&mapSizeH),sizeof(mapSizeH));	//write map height

	for(int x = 0; x < tiles.size(); x++)
	{
		for(int y = 0; y < tiles[y].size(); y++)
		{
			TileEntity entity = tiles[x][y];
			binary_file.write(reinterpret_cast<char *>(&entity), sizeof(entity));	//write tile value
		}
	}

	binary_file.close();
	file_op.close();
}

Map SaveFile::loadMap(string filename)
{
	fstream file_op(filename.c_str(), ios::in);
	vector<vector<TileEntity>> map = vector<vector<TileEntity>>();
	int background = 0;
	if(file_op.is_open())
	{
		fstream binary_file(filename.c_str(), ios::in|ios::binary);
		
		int mapSizeW = 0;
		int mapSizeH = 0;
		binary_file.read(reinterpret_cast<char *>(&background),sizeof(background)); //read map width
		binary_file.read(reinterpret_cast<char *>(&mapSizeW),sizeof(mapSizeW)); //read map width
		binary_file.read(reinterpret_cast<char *>(&mapSizeH),sizeof(mapSizeH)); //read map height		

		for(int x = 0; x < mapSizeW; x++)
		{
			vector<TileEntity> row;
			for(int y = 0; y < mapSizeH; y++)
			{
				TileEntity entity = TileEntity();
				binary_file.read(reinterpret_cast<char *>(&entity), sizeof(entity)); //read tile value
				row.push_back(entity);
			}
			map.push_back(row);
		}
	}
	file_op.close();

	Map output = Map(background, map);
	return output;
}