#pragma once
#include "gameobject.h"
class TankTrap :
	public GameObject
{
public:
	static TextureRegion* trapRegion;

	TankTrap(void);
	~TankTrap(void);

	TankTrap (double x, double y, double width, double height, double rotation);

	void performCollision(GameObject* otherObject, float delta);

	TextureRegion* createTrapRegion();
};

