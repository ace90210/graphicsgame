#pragma once

      

class Vector2
{
public:	
	static const double TO_RADIANS;	
	static const double TO_DEGREES;

	double x;
	double y;

	Vector2(void);
	Vector2(double x, double y);

	~Vector2(void);

	void add(Vector2 v);

	void add(double x, double y);

	void subtract(Vector2 v);

	void subtract(double x, double y);

	void rotate(double angle);

	static long double distance(Vector2 v1, Vector2 v2);

	static long double getAngle(Vector2 v1, Vector2 v2);

	static float Q_rsqrt( float number );
};

