//
// The structure of the Graphics 1 OpenGL template is explained in README.txt
//
/*

TO-DO LIST
----------
- add SAT collision detection - done
- automatc collision detection system (collision grid) //done except grid is not implemented yet
- collision response class //done kinda (dont seem to be able to do my ideal way in c++)
- add aim at cursor to tank - done
- add tile class
- add tile map class
- add fonts - done
- asset manager ---- try (again)
- sound	//done one sample test thats working
- effects class
- timer class
- timer response class
- level class
- ai class

GameActivity implementation


The comments in the file Activity.cpp give a little more info about each method

This activity is where the main game logic goes.
Most of your game code will go here
*/


#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include <math.h>       /* tan */

#include "SOIL.h"



#include "OpenGLApplication.h"			// Needed to access member functions and variables from OpenGLApplication
#include "GameActivity.h"
#include "Collision.h"
#include "Map.h"
#include "Repair.h"

#include "SaveFile.h"

#include "freetype.h"		// Header for font library.
using namespace freetype;

// This holds all the information for the font that we are going to create.
font_data our_font;
font_data our_font2;

font_data GameActivity::hp_font;

#define PI 3.14159265

#define WORLD_WIDTH 4
#define WORLD_HEIGHT 4

#define CAMERA_MOVEMENT_SPEED 15.0
#define PLAYER_MOVEMENT_SPEED 1.0
#define DEFAULT_TRAP_SIZE 2
#define STARTING_TRAP_COUNT 100
#define STARTING_MINE_COUNT 50
#define THREAT_SOUND_RESUME 4
#define REPAIR_DEFAULT 20
#define REPAIR_SIZE 1.5
#define START_HEALTH 75


#define MUSIC_VOLUME 0.3
#define EXPLOSION_SOUND_RANGE 35
#define MIN_EXPLOSION_VOLUME 0.2
#define PLAYER_TILE_ID -1

#define ACCELERATION 15
#define TURN_SPEED 50
#define TURRET_TURN_SPEED 80

#define LEVEL_LIMIT 2
#define WAVE_LIMIT 3

double ratio;

unsigned int GameActivity::score = 0;
int GameActivity::wave = 1;
int GameActivity::level = 1;

double GameActivity::aspectRatio = 1;
double GameActivity::camX = 0;
double GameActivity::camY = 0;


double GameActivity::screenWidth = 0;
double GameActivity::screenHeight = 0;
double GameActivity::timeSinceThreatPlayed = 0;

int GameActivity::seenByTanks = 0;

CollisionGrid GameActivity::grid;
std::vector<Nuke*> GameActivity::explosions;



TileEntity GameActivity::map[mapW][mapH];

SoundEffect GameActivity::musicNormal;
SoundEffect GameActivity::musicThreat;

//Nuke GameActivity::testNuke;


GameActivity::GameActivity(OpenGLApplication *app)
	: Activity(app)		// Call super constructor
{
	GameActivity::aspectRatio = app->getAspectRatio();
	grid = CollisionGrid();
	playerWins = false;
	nextWave = false;
	nextLevel = false;
	healthText = true;
	
	// Initialse camera and player positions to be a the origin
	camX = camY = 0.0;
	time = 0;
	inventoryTraps = STARTING_TRAP_COUNT;
	inventoryMines = STARTING_MINE_COUNT;

	shotsFired = 0;
	totalEnemyHealth = 0;
	level = 1;
	wave = 1;
	//makeTileMap();

	tankResetPosition = Vector2(0, 0);
	
	layer0Objects = std::vector<GameObject*>();
	layer1Objects = std::vector<GameObject*>();
	layer2Objects = std::vector<GameObject*>();
	layer3Objects = std::vector<GameObject*>();

	dynamicTileObjects = std::vector<DynamicGameObject*>();

	GameObject::debug = ALL;
}


/*
*
* ACTIVITY METHODS
* Put your application/game code here
*
*/
void GameActivity::initialise()
{
	// Initialise the activity; called at application start up
	
	Tank* tmp = new Tank(3, 3.0, 2, 2, 45, START_HEALTH, true, 10, &grid);
	tank = tmp;
	grid.registerObject(tmp);
	this->camFollow = true;

	our_font.init("arialbd.TTF", 14);					    //Build the freetype font

	our_font2.init("arialbd.TTF", 48 );			   //Build the freetype font
	hits = 0;


	backgrounds = vector<Background*>();
	backgrounds.push_back(new Background("backgroundsmall.jpg", -VIEW_SIZE * (WORLD_WIDTH / 2), -VIEW_SIZE * (WORLD_HEIGHT / 2), WORLD_WIDTH * VIEW_SIZE, WORLD_HEIGHT * VIEW_SIZE));
	backgrounds.push_back(new Background("backgroundsmall2.png", -VIEW_SIZE * (WORLD_WIDTH / 2), -VIEW_SIZE * (WORLD_HEIGHT / 2), WORLD_WIDTH * VIEW_SIZE, WORLD_HEIGHT * VIEW_SIZE));

	/*backgrounds.push_back(new Background("backgroundlarge.jpg", -VIEW_SIZE * (WORLD_WIDTH / 2), -VIEW_SIZE * (WORLD_HEIGHT / 2), WORLD_WIDTH * VIEW_SIZE, WORLD_HEIGHT * VIEW_SIZE));
	backgrounds.push_back(new Background("backgroundlarge2.png", -VIEW_SIZE * (WORLD_WIDTH / 2), -VIEW_SIZE * (WORLD_HEIGHT / 2), WORLD_WIDTH * VIEW_SIZE, WORLD_HEIGHT * VIEW_SIZE));*/

	background = backgrounds[1];

	leftWall =   InvisibleWall(Vector2( background->x - 0.5                    , background->y + (background->height / 2) ), 1, background->height, 0);	
	rightWall =  InvisibleWall(Vector2( background->x + background->width + 0.5 , background->y + (background->height / 2) ), 1, background->height, 0);

	topWall =    InvisibleWall(Vector2( background->x + (background->width / 2), background->y + background->height  + 0.5), background->width + 2, 1 , 0);
	bottomWall = InvisibleWall(Vector2( background->x + (background->width / 2), background->y - 0.5                     ), background->width + 2, 1 , 0);

	grid.registerObject(&leftWall);
	grid.registerObject(&rightWall);
	grid.registerObject(&topWall);
	grid.registerObject(&bottomWall);



	loadTileMap(1,1);
	//genTileMap(background->x, background->y, background->width, background->height);


	musicNormal = SoundEffect("musicNorm.mp3");
	musicNormal.setGlobalVolume(MUSIC_VOLUME);
	musicThreat = SoundEffect("musicThreat.mp3");
	musicThreat.setGlobalVolume(MUSIC_VOLUME);

	

	//testNuke = Nuke(0, 0, 4, 4, 0.15);
}


void GameActivity::shutdown()
{
	// Shutdown the activity; called at application finish

	// Delete the texture
	//glDeleteTextures(1, &playerTextureID);
}



void GameActivity::onSwitchIn()
{
	// Activity switch in


	// EXAMPLE CODE
	glClearColor(0.0,0.0,0.0,0.0);						//sets the clear colour to black
	musicNormal.Play();
}

void GameActivity::onReshape(int width, int height)
{
 	ratio = (double)width / (double)height;
	screenWidth = width;
	screenHeight = height;
	// If you need to do anything when the screen is resized, do it here

	// EXAMPLE CODE
	glViewport(0,0,width,height);						// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	double aspect = app->getAspectRatio();
	GameActivity::aspectRatio = aspect;
	// The height of the visible area is defined by VIEW_SIZE. Split it half each way around the origin, hence the *0.5
	// Take the aspect ratio into consideration when computing the width of the visible area
	gluOrtho2D(-VIEW_SIZE*0.5*aspect, VIEW_SIZE*0.5*aspect,  -VIEW_SIZE*0.5, VIEW_SIZE*0.5);

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix

	//rescale font
	double scaleX = this->app->getScreenWidth() / (VIEW_SIZE * ratio);
	our_font2.init("arialbd.TTF", (int)(1.5*scaleX) );			   //Build the freetype font
	hp_font.init("arialbd.TTF", (int)( scaleX * 0.5 ));					    //Build the freetype font

	generateGui(); //regerneate gui items as scale likely has changed
}

void GameActivity::update(double deltaT, double prevDeltaT)
{
	fps = 1 / deltaT;
	GameActivity::timeSinceThreatPlayed += deltaT;
	if(!playerWins && !nextWave && !nextLevel)
	{
		//if player not won yet		

		time = deltaT;
		// Update the application; if the current frame is frame number F, then the previous frame is F-1 and the one before that is F-2
		// deltaT is the time elapsed from frame F-1 to frame F, prevDeltaT is the time elapsed from F-2 to F-1

		// Update the simulation here
		// TRY NOT TO DO ANY RENDERING HERE
		// step the simulation forward by the amount of time specified by deltaT
		//
		// If you need to do different things depending on whether or not keys are pressed, etc,
		// get the member variable inputState and use its isKeyPressed(), getMouseX(), getMouseY() and isMouseButtonPressed() methods
		// to determine the state of the keys


		// Arrow keys control camera
		// NOTE the use of deltaT here; we move the position of the camera by the SPEED * deltaT

		if(inputState->isKeyPressed(VK_NUMPAD8) || inputState->isKeyPressed('W'))
		{
			tank->increaseSpeed( ACCELERATION * deltaT);
		}else if(inputState->isKeyPressed(VK_NUMPAD2) || inputState->isKeyPressed('S'))
		{
			tank->decreaseSpeed( ACCELERATION * deltaT);
		}
		if(inputState->isKeyPressed(VK_NUMPAD6) || inputState->isKeyPressed('D'))
		{
			tank->setTurn( TURN_SPEED );
		}else if(inputState->isKeyPressed(VK_NUMPAD4) || inputState->isKeyPressed('A'))
		{
			tank->setTurn( -TURN_SPEED);
		}
		else
		{
			tank->stopTurn();
		}

		if(inputState->isKeyPressed(VK_NUMPAD9) || inputState->isKeyPressed('E'))
		{
			tank->rotateTurret(  TURRET_TURN_SPEED * deltaT);
		}
		else if(inputState->isKeyPressed(VK_NUMPAD7) || inputState->isKeyPressed('Q'))
		{
			tank->rotateTurret(  -(TURRET_TURN_SPEED * deltaT));
		}
	
	
		if(inputState->isKeyPressed(VK_LEFT))
		{
			camX -= CAMERA_MOVEMENT_SPEED * deltaT;
		}
		if(inputState->isKeyPressed(VK_RIGHT))
		{
			camX += CAMERA_MOVEMENT_SPEED * deltaT;
		}
		if(inputState->isKeyPressed(VK_UP))
		{
			camY += CAMERA_MOVEMENT_SPEED * deltaT;
		}
		if(inputState->isKeyPressed(VK_DOWN))
		{
			camY -= CAMERA_MOVEMENT_SPEED * deltaT;
		}

		
			
		//if(!SAT2D(&player.boundingBox, &animation.boundingBox)){
		//	player.setPosition(10, 10);
		//}
		hits = grid.update(deltaT);
		//animation->update(deltaT);
		tank->update(deltaT);

		//update all dynamic tile entities
		for(unsigned int i = 0; i < dynamicTileObjects.size(); i++)
		{
			dynamicTileObjects[i]->update(deltaT);
		}

		int leftAlive = 0;
		seenByTanks = 0;
		for(unsigned int i = 0; i < enemies.size(); i++)
		{
			enemies[i]->update(deltaT);
			if(enemies[i]->isAlive())
			{
				leftAlive++;
			}
		}
		//if seen by more ha one tank and threat music not playing play it
		if(seenByTanks > 0 && (!musicThreat.isPlaying() || musicThreat.isPaused()))
		{
			GameActivity::startThreatMusic();
		}
		else if(seenByTanks == 0 && (!musicNormal.isPlaying() || !musicThreat.isPaused()))
		{
			//thrat music playing but no one sees us stop it playing
			GameActivity::stopThreatMusic();
		}

		//if none left aline game over
		if(leftAlive == 0)
		{
			wave++;
			if(wave > WAVE_LIMIT)
			{
				wave = 1;
				level++;
				if(level > LEVEL_LIMIT)
				{
					playerWins = true;
					app->setCurrentActivity(app->winScreen);
					return;
				}
				else
				{
					nextLevel = true;
				}
			}
			else
			{
				nextWave = true;
			}			
		}

	
		if(camFollow)
		{
			camX = tank->getPosition().x;
			camY = tank->getPosition().y;
		}
	}
	for(unsigned int i = 0; i < testMines.size(); i++)
	{
		testMines[i]->update(deltaT);		//draw before (under) tanks
	}	
	//testNuke.update(deltaT);
	//testMine.update(deltaT);

	updateExplosions(deltaT);

	//SoundEffect::Update();
	
	//check if player dead
	if(!tank->isAlive())
	{
		//stop all sounds from tanks
		tank->stopAllSounds();
		for(unsigned int i = 0; i < enemies.size(); i++)
		{
			enemies[i]->stopAllSounds();
		}
		if(musicNormal.isPlaying())
		{
			musicNormal.Stop();
		}
		if(musicThreat.isPlaying())
		{
			musicThreat.Stop();
		}
		app->setCurrentActivity(app->endScreen);
	}
}

void GameActivity::render()
{
	glClear(GL_COLOR_BUFFER_BIT);
	
	glLoadIdentity();									// Reset The Modelview Matrix
	// Use the negated camera position as a translation; effectively we move the world and the camera so that the camera is at 0,0,0
	glTranslated(-camX, -camY, 0.0);

	background->draw();

	if(Tank::debug == ALL) //only show in all debug mode
	{
		// Render the grid in the background
		renderDebugGrid(background->x, background->y, background->width, background->width, VIEW_SIZE, VIEW_SIZE);
	}

	//draw layer 0 objects
	for(unsigned int i = 0; i < layer0Objects.size(); i++)
	{
		layer0Objects[i]->draw();
	}

	
	// player

	//animation->draw();

	for(unsigned int i = 0; i < testTraps.size(); i++)
	{
		testTraps[i]->draw();		//draw before (under) tanks
	}	

	
	for(unsigned int i = 0; i < testMines.size(); i++)
	{
		testMines[i]->draw();		//draw before (under) tanks
	}	


	//draw layer 1 objects
	for(unsigned int i = 0; i < layer1Objects.size(); i++)
	{
		layer1Objects[i]->draw();
	}
	for(unsigned int i = 0; i < enemies.size(); i++)
	{
		enemies[i]->draw();		
	}

	

	//draw layer 2 objects
	for(unsigned int i = 0; i < layer2Objects.size(); i++)
	{
		layer2Objects[i]->draw();
	}


	leftWall.draw();   //only really shows debug stuff
	rightWall.draw();  //only really shows debug stuff
	topWall.draw();    //only really shows debug stuff
	bottomWall.draw(); //only really shows debug stuff

	//testMine.draw();


	//player.draw();
	tank->draw();

	//draw layer 3 objects
	for(unsigned int i = 0; i < layer3Objects.size(); i++)
	{
		layer3Objects[i]->draw();
	}

	//if(testNuke.play)
	//{
	//	testNuke.draw();
	//}
	tank->drawBullets();
	for(unsigned int i = 0; i < enemies.size(); i++)
	{
		enemies[i]->drawBullets();		
	}

	for(unsigned int i = 0; i < explosions.size(); i++)
	{
		explosions[i]->draw();		
	}

	glLoadIdentity();	
	glColor3f(1.0,1.0,1.0);
	double scale = (this->app->getScreenHeight() / VIEW_SIZE) / BASE_SCALE;


	print(our_font, scale, 0.90f, 0.4f, 0.4f, 0, (this->app->getScreenHeight() - (14 * scale)),  "Level: %1d, Wave: %1d, Score: %d   --- FPS: %2.0f", this->level, this->wave, GameActivity::score, fps);

	
	print(our_font, scale,  0, (this->app->getScreenHeight() - (45 * scale)), "Current Collision debugging state: %s, Press M to debug change modes", GameObject::getDebugName().c_str());

//if(playerWins)
//	{
//		if(shotsFired != 0)
//		{
//			double accuracy = (double)totalEnemyHealth / (double)shotsFired;
//			glColor3f(1.0,0.0,0.0);
//			print(our_font, scale, this->app->getScreenWidth() / 20, this->app->getScreenHeight() / 2 , "You win with a score of %d + bonus of %3.1f%% = %.0f", this->score, accuracy * 100, (double)this->score + (accuracy * (double)this->score));
//		}
//		else
//		{
//			//nice job got ai to kill themselves
//			glColor3f(1.0,0.0,0.0);
//			print(our_font, scale, this->app->getScreenWidth() / 20, this->app->getScreenHeight() / 2 , "You win with a score of %d + bonus for not using your weapons of 200%% = %d", this->score, (double)this->score + (2 * (double)this->score));
//		}
//	}
//	else
	if(nextWave)
	{
		print(our_font, scale, 1, 0, 0, this->app->getScreenWidth() / 20, this->app->getScreenHeight() / 2 , "Wave %d  complete press enter to play next wave, score: %d", (wave - 1), this->score);
	}
	else if(nextLevel)
	{
		print(our_font, scale, 1, 0, 0, this->app->getScreenWidth() / 20, this->app->getScreenHeight() / 2 , "Level %d complete press enter to play next level, score: %d",  (level - 1), this->score);
	}

	if(healthText)
	{
		tank->drawHealthNumbers();
		for(unsigned int i = 0; i < enemies.size(); i++)
		{
			if(enemies[i]->isAlive())
			{
				enemies[i]->drawHealthNumbers();		
			}
		}
	}

	if(musicThreat.isPlaying() && !musicThreat.isPaused())
	{
		print(our_font, scale, 1, 0, 0, this->app->getScreenWidth() / 3, this->app->getScreenHeight() / 10 , "Your being chased get KILL them");
	}
	
	//draw gui
	//draw layer 3 objects
	for(unsigned int i = 0; i < guiItems.size(); i++)
	{
		guiItems[i]->draw();
	}
	//print num of traps in inventory
	print(our_font, scale, 35 * scale, 25 * scale , "%d      x traps", inventoryTraps );
	print(our_font, scale, 180 * scale, 25 * scale , "%d    x mines", inventoryMines );
	glFlush();

}

void GameActivity::addToScore(unsigned int amount)
{
	GameActivity::score += amount;
}


void GameActivity::onMouseDown(int button, int mouseX, int mouseY)
{
	if(!playerWins)
	{
		
		// This method will be invoked when a mouse button is pressed
		// button: 0=LEFT, 1=MIDDLE, 2=RIGHT
		// mouseX and mouseY: position
		double scaleX = this->app->getScreenWidth() / (VIEW_SIZE * ratio) ;
		double mX = ((((mouseX - (this->app->getScreenWidth() / 2 )) / scaleX))) + camX ;

		double scaleY = this->app->getScreenHeight() / VIEW_SIZE;
		double mY = ((((mouseY - (this->app->getScreenHeight() / 2 )) / scaleY))) + camY ;
		this->mouseX = mX;
		this->mouseY = mY;

		this->TankX = tank->getPosition().x;

		double distance = Vector2::distance( Vector2(mX, mY) , this->tank->getPosition() );

		this->distanceX = distance;
		if(button == 0)
		{
			if(this->tank->fire( distance ))
			{
				shotsFired++;
			}
		}
	}
}

void GameActivity::onMouseUp(int button, int mouseX, int mouseY)
{
	// This method will be invoked when a mouse button is released
	// button: 0=LEFT, 1=MIDDLE, 2=RIGHT
	// mouseX and mouseY: position
}

void GameActivity::onMouseMove(int mouseX, int mouseY)
{
	if(!playerWins)
	{
		// This method will be invoked when the mouse is moved
		// mouseX and mouseY: position
		double scaleX = this->app->getScreenWidth() / (VIEW_SIZE * ratio);
		double mX = ((((mouseX - (this->app->getScreenWidth() / 2 )) / scaleX))) + camX ;

		double scaleY = this->app->getScreenHeight() / VIEW_SIZE;
		double mY = ((((mouseY - (this->app->getScreenHeight() / 2 )) / scaleY))) + camY ;
		this->mouseX = mX;
		this->mouseY = mY;

		double x1 = mX - (this->tank->getPosition().x );
		double y1 = mY - (this->tank->getPosition().y);
		if(y1 != 0){
			double angle = atan2 (x1, y1) * 180 / PI;
			this->tank->setTurretRotation(angle);
		}
	}
}

void GameActivity::onKeyDown(int key)
{
	if(nextLevel || nextWave)
	{
		//if awaiting user input for next wave/level
		if(key == VK_RETURN)
		{
			if(nextWave)
			{
				nextWave = false;
				loadTileMap(level, wave);
			}
			else
			{
				if(level - 1 < LEVEL_LIMIT)
				{
					resetMap();
					loadTileMap(level, wave);
				}
				else
				{
					playerWins = true;
				}
				nextLevel = false;
			}
		}
	}else if(!playerWins)
	{
		// This method will be invoked when a key is pressed
		if(key == 'C')
		{
			camFollow = !camFollow; // toggle cam follow mode
		}
		else if(key == VK_NUMPAD5 || key == VK_SPACE)
		{		
			if(tank->fire())
			{
				shotsFired++;
			}
		}else if(key == 'M')
		{		
			GameObject::cycleDebugModes();
		}else if(key == VK_NUMPAD9 || key == VK_NUMPAD7)
		{
			tank->playTurretSound();
		}
		else if(key == VK_RETURN && inventoryTraps > 0)
		{
			vector<int> isCollidingWith = grid.getCollisionsWith(tank);

			bool isTouchingTrap = false;
			for(int i = 0; i < isCollidingWith.size(); i++)
			{
				if(isCollidingWith[i] == 8) // 8 = trap id
				{
					isTouchingTrap = true; 
					break;
				}
			}

			if(!isTouchingTrap)
			{
				//only if not alrady touching a trap allow placement
				inventoryTraps--;
				TankTrap* newTrap = new TankTrap(tank->getPosition().x, tank->getPosition().y, DEFAULT_TRAP_SIZE, DEFAULT_TRAP_SIZE, tank->getRotation() + 90);
				testTraps.push_back(newTrap);
				grid.registerObject(newTrap);
			}
		}
		else if(key == 'N' && inventoryMines > 0 && this->tank->speed >= 0)
		{
			//calc offset
			double MINE_SIZE = 1;
			Vector2 minePosition = this->tank->getPosition();
			double currentX = minePosition.x;
			double currentY = minePosition.y;
			double distance = -((this->tank->height / 2) + (MINE_SIZE / 2)) ; 
		

			double rad =  this->tank->getRotation() * DynamicGameObject::TO_RADIANS;
			double distanceX = sin(rad) * distance;
			double distanceY = cos(rad) * distance;
			minePosition.add(distanceX, distanceY);

			Mine* newMine = new Mine(minePosition.x, minePosition.y, MINE_SIZE, MINE_SIZE, tank->getRotation() + 90, &grid);
		

			vector<int> isCollidingWith = grid.getCollisionsWith(newMine);

			
			if(isCollidingWith.size() == 0) // 8 = trap id
			{
				//only if not alrady touching a trap allow placement
				inventoryMines--;
				grid.registerObject(newMine);
				testMines.push_back(newMine);
				shotsFired++;
			}
		}else if(key == 'H')
		{
			healthText = !healthText; //toogle health bar text
		}
	}
}

void GameActivity::onKeyUp(int key)										// Called when key released
{
	// Key released

   if(key == 'P')
	{
		musicNormal.TogglePause();
	}
}


void GameActivity::genTileMap(float left, float bottom, float width, float height)
{
	double tileWidth = width / mapW;
	double tileHeight = height / mapH;

	//initialise layers	

	for(unsigned int x = 0; x < mapW; x++)
	{
		for(unsigned int y = 0; y < mapH; y++)
		{
			TileEntity entity = map[y][x];
			double entityX = left + (tileWidth / 2) + x * tileWidth;
			double entityY = bottom + (tileHeight / 2) + y * tileHeight;

			double entityWidth = entity.width;
			double entityHeight = entity.height;
			
			if(!entity.absoluteSize)
			{
				//if size is relative change size of entity
				entityWidth *= tileWidth;
				entityHeight *= tileHeight;
			}

			switch(entity.itemId)
			{
				case PLAYER_TILE_ID: 
						{
							tankResetPosition = Vector2(entityX, entityY);
							tank->setPosition(entityX, entityY);
						 };
						break;
				case TRAP: {
							

							TankTrap* trap = new TankTrap(entityX, entityY, entityWidth, entityHeight, 0);
							testTraps.push_back(trap);
							
							registerStaticEntity(trap, entity.layer);
						} break;
				case TANK: {					

							NPCTank* newNpc = new NPCTank(entityX, entityY, entity.rotation, entity.health, entity.speed, entity.fireRate, &grid, tank);							
							//registerDynamicEntity(newNpc, entity.layer);

							//tanks have own vector list
							enemies.push_back(newNpc);
							grid.registerObject(newNpc);
							totalEnemyHealth += newNpc->maxLife;
						} break;
				case WALL: 
					{
						Wall* wall = new Wall(entityX, entityY, entityWidth, entityHeight, entity.rotation);							
						registerStaticEntity(wall, entity.layer);
					} break;
				case REPAIR: 
					{
						Repair* testRepair = new Repair(REPAIR_DEFAULT, entityX, entityY, REPAIR_SIZE, REPAIR_SIZE, 0);
						registerStaticEntity(testRepair, 0);
					}

			}		

		}
	}
}

void GameActivity::registerStaticEntity(GameObject* entity, int layer)
{
	switch(layer)
	{
		case 1: {	layer1Objects.push_back(entity);	} break;
		case 2: {	layer2Objects.push_back(entity);	} break;
		case 3: {	layer3Objects.push_back(entity);	} break;
		default: {	layer0Objects.push_back(entity);	} break;
	}
	grid.registerObject(entity);
}

void GameActivity::registerDynamicEntity(DynamicGameObject* entity, int layer)
{
	this->registerStaticEntity(entity, layer);
	this->dynamicTileObjects.push_back(entity);
}

void GameActivity::renderDebugGrid(float left, float bottom, float width, float height, int hSegments, int vSegments)
{
	// Render a grey grid using lines
	glColor3f(0.4f, 0.4f, 0.4f);
	glBegin(GL_LINES);

	// Vertical lines
	float dx = width / hSegments;
	float x = left;
	for (int i = 0; i <= hSegments; i++)
	{
		glVertex2f(x, bottom);
		glVertex2f(x, bottom + height);

		x += dx;
	}
	
	// Horizontal lines
	float dy = height / vSegments;
	float y = bottom;
	for (int i = 0; i <= hSegments; i++)
	{
		glVertex2f(left, y);
		glVertex2f(left + width, y);

		y += dy;
	}

	glEnd();
}


void GameActivity::loadTileMap(int level, int wave)
{
	vector<vector<TileEntity>> outputMap(mapH);
	Map newMap = SaveFile::loadMapLevel(level, wave);
	outputMap = newMap.tiles;

	background = this->backgrounds[newMap.backgroundId];

	for(int x = 0; x < mapW; x++)
	{
		for(int y = 0; y < mapH; y++)
		{
			map[x][y] = outputMap[x][y];
		}
	}
	tank->setPosition(tankResetPosition.x, tankResetPosition.y);
	genTileMap(background->x, background->y, background->width, background->height);
}

void GameActivity::resetMap()
{
	//reset map collisions for all objects (except player and invisible walls
				
	
	for(unsigned int i = 0; i < layer0Objects.size(); i++)
	{
		grid.unregisterObject(layer0Objects[i]);
	}
	for(unsigned int i = 0; i < layer1Objects.size(); i++)
	{
		grid.unregisterObject(layer1Objects[i]);
	}
	for(unsigned int i = 0; i < layer2Objects.size(); i++)
	{
		grid.unregisterObject(layer2Objects[i]);
	}
	for(unsigned int i = 0; i < layer3Objects.size(); i++)
	{
		grid.unregisterObject(layer3Objects[i]);
	}
	for(unsigned int i = 0; i < dynamicTileObjects.size(); i++)
	{
		grid.unregisterObject(dynamicTileObjects[i]);
	}

	for(unsigned int i = 0; i < testTraps.size(); i++)
	{
		grid.unregisterObject(testTraps[i]);
	}	
	for(unsigned int i = 0; i < testMines.size(); i++)
	{
		grid.unregisterObject(testMines[i]);
	}	
	for(unsigned int i = 0; i < enemies.size(); i++)
	{
		grid.unregisterObject(enemies[i]);
	}
	for(unsigned int i = 0; i < explosions.size(); i++)
	{
		grid.unregisterObject(explosions[i]);
	}


	testTraps.clear();
	testMines.clear();

	layer0Objects = std::vector<GameObject*>();
	layer1Objects = std::vector<GameObject*>();
	layer2Objects = std::vector<GameObject*>();
	layer3Objects = std::vector<GameObject*>();

	dynamicTileObjects = std::vector<DynamicGameObject*>();

	
}

//make a new map with tile entities
void GameActivity::makeTileMap()
{

	//make emty map
	for(int x = 0; x < mapW; x++)
	{
		for(int y = 0; y < mapH; y++)
		{
			map[y][x] = TileEntity();
		}
	}

	//add elements here
	TileEntity trap = TileEntity(TRAP, 0, 5, 4, 4, true);
	TileEntity npc = TileEntity(TANK, 1, 10, 0.3, 0, 0, 10, -75, true);
	TileEntity wall = TileEntity(WALL, 0, 1, 1, 0.25, 0, 0, false);
	TileEntity playerStart = TileEntity(PLAYER_TILE_ID);
	TileEntity repair = TileEntity(REPAIR);

	
	//gen wave 1 level 1
	map[1][1] = playerStart;

	map[2][1] = wall;
	map[2][0] = wall;
	
	wall.rotation = 90;
	map[1][2] = wall;
	map[0][2] = wall;

	map[0][0] = repair;
	map[9][9] = repair;
	map[0][9] = repair;
	map[9][0] = repair;

	map[7][7] = npc;
	map[6][8] = npc;
	
	
	//wave 1 level  2
	/*map[5][5] = playerStart;

	map[4][5] = wall;
	map[4][6] = wall;
	
	map[7][5] = wall;
	map[7][6] = wall;

	map[0][0] = repair;
	map[9][9] = repair;
	map[0][9] = repair;
	map[9][0] = repair;
	
	
	wall.rotation = 90;
	map[5][4] = wall;
	map[6][4] = wall;
	
	map[5][7] = wall;
	map[6][7] = wall;

	map[7][7] = npc;
	map[6][8] = npc;*/
	

	

//wave 2 level 1, 2
	/*map[7][7] = npc;
	map[6][8] = npc;
	

	map[0][1] = npc;
	map[2][8] = npc;

	map[8][2] = npc;
	
	map[0][0] = repair;
	map[9][9] = repair;
	map[0][9] = repair;
	map[9][0] = repair;*/
	
	

	
	
	//wave 3 level 1, 2
	/*map[7][7] = npc;
	map[6][8] = npc;
	
	map[0][1] = npc;
	map[1][0] = npc;

	map[0][0] = repair;
	map[9][9] = repair;
	map[0][9] = repair;
	map[9][0] = repair;


	npc.health = 25;
	map[8][2] = npc;
	map[8][3] = npc;
	map[2][8] = npc;
	map[3][8] = npc;

	npc.health = 50;
	map[3][3] = npc;*/

	vector<vector<TileEntity>> outputMap(mapH);
	
	
	for(int i = 0; i < mapH; i++)
	{
		outputMap[i].assign(map[i], map[i] + mapW);
	}
	Map saveMap = Map(0, outputMap);
	SaveFile::saveMapLevel(1, 1, saveMap);

}


void GameActivity::generateGui()
{
	//create gui items
	guiItems = vector<GameObject*>();
	double scale = (this->app->getScreenHeight() / VIEW_SIZE) / BASE_SCALE;

	TankTrap* guiTankTrap = new TankTrap(0.5 / aspectRatio, -0.4, 4, 4, 0);
	registerGuiItem(guiTankTrap);

	Mine* giuMine = new Mine(7.5 / aspectRatio, -0.4, 4, 4, 0, &grid);
	registerGuiItem(giuMine);
}

void GameActivity::registerGuiItem(GameObject* guiItem)
{
	//recalc correct gui location	
	double halfWidth = (VIEW_SIZE * aspectRatio) / 2;
	double halfHeight = VIEW_SIZE  / 2;
	
	double guiX =  (guiItem->getPosition().x * aspectRatio) - halfWidth + (guiItem->width / 2);
	
	double guiY = guiItem->getPosition().y - halfHeight + (guiItem->height / 2);

	guiItem->setPosition(guiX, guiY);


	guiItems.push_back(guiItem);
}

void GameActivity::startThreatMusic()
{
	if(musicNormal.isPlaying())
	{
		musicNormal.Stop();
	}

	if(GameActivity::timeSinceThreatPlayed > THREAT_SOUND_RESUME && musicThreat.isPlaying())
	{
		//been long enough restart threat from begining
		GameActivity::timeSinceThreatPlayed = 0;
		musicThreat.Stop();
		musicThreat.Play();
	}
	else
	{
		

		if(!musicThreat.isPlaying())
		{
 			musicThreat.Play();
		}
		else
		{
			musicThreat.SetPause(false);
		}
	}
}

void GameActivity::stopThreatMusic()
{
	if(musicThreat.isPlaying())
	{
		musicThreat.SetPause(true);
		GameActivity::timeSinceThreatPlayed = 0;
	}


	if(!musicNormal.isPlaying())
	{
		musicNormal.Play();
	}
}

void GameActivity::addSeenByTank()
{
	seenByTanks++;
}

void  GameActivity::updateExplosions(double delta)
{
	vector<Nuke*>::iterator it = explosions.begin();

	while(it != this->explosions.end())
	{
		Nuke* explosion = *it;
		if(explosion->done && explosion->play){
			//has exploded and ended delete
			explosion->unload();
			grid.unregisterObject(explosion);
			it = this->explosions.erase(it);
		}
		else if(!explosion->play)
		{
			double camDistance = Vector2::distance(explosion->getPosition(), Vector2(GameActivity::camX, GameActivity::camY));
			double vol = 1 - (camDistance / EXPLOSION_SOUND_RANGE);
			if(vol < MIN_EXPLOSION_VOLUME)
			{
				vol = MIN_EXPLOSION_VOLUME;
			}
			explosion->startPlayback(vol);
			it++;
		}else if(!explosion->done){
			double camDistance = Vector2::distance(explosion->getPosition(), Vector2(GameActivity::camX, GameActivity::camY));
			double vol = 1 - (camDistance / EXPLOSION_SOUND_RANGE);
			if(vol < MIN_EXPLOSION_VOLUME)
			{
				vol = MIN_EXPLOSION_VOLUME;
			}
			explosion->explosion->SetVolume(vol);
			it++;
		}
		explosion->update(delta);
	}
}


void GameActivity::addExplosion(double x, double y)
{
	Nuke* newExplosion = new Nuke(x, y, 1.5, 1.5, 0.016667);
	explosions.push_back(newExplosion);
	grid.registerObject(newExplosion);
}