#pragma once

#include <vector>
#include "TileEntity.h"
#include "Map.h"

using namespace std;

class SaveFile
{
private:
	static void saveMap(string filename, Map map);
	static Map loadMap(string filename);
public:
	SaveFile(void);
	~SaveFile(void);

	static void saveMap(vector<vector<int>> map);

	static vector<vector<int>> loadMap();

	static void saveMapS(Map map);

	static Map loadMapS();

	static void saveMapLevel(int level, Map map);

	static Map loadMapLevel(int level);

	static void saveMapLevel(int level, int wave, Map map);

	static Map loadMapLevel(int level, int wave);
};

