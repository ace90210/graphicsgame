#include "Turret.h"

Turret::Turret(double x, double y, double width, double height, double rotation, TextureRegion* textureRegion, int maxLife, bool solid, double speed)
	: DynamicGameObject( x, y, width,  height,  rotation,  textureRegion,  maxLife,  solid, speed)
{
	
	collisionId = TANK;
	ignoreCollisionsWith = BULLET;
	
}

Turret::Turret(void)
{
}


Turret::~Turret(void)
{
}


void Turret::performCollision(GameObject* otherObject, float delta)
 {
	 
 }