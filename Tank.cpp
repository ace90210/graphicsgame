#include "Tank.h"
#include <Windows.h>
#include <MMSystem.h>

#include "GameActivity.h"
#include "Collision.h"


// This holds all the information for the font that we are going to create.
font_data Tank::hp_font;


#define MAX_DISTANCE 40
#define RELOAD_TIME 0.3
#define MAX_TURRET_TURN_SPEED 45
#define TURRET_TURN_SPEED 100
#define TURRET_THRESHOLD 0.1
#define TURRET_TURN_SOUND_THRESHOLD 0.5 //be at least 0.5 degrees before playing sound
#define BULLET_SIZE 0.4
#define BULLET_SPEED 25

#define BULLET_VOLUME 0.7
#define MAX_SPEED_VOLUME 1
//#define TURRET_VOLUME 1 //too much trouble temp disabled
#define MAX_CRASH_VOLUME 1
#define MIN_CRASH_VOLUME 0.1
#define GLOBAL_TANK_VOLUME 0.5
#define SOUND_RANGE 30

#define HP_SHADOW_OFFSET_X 0.083
#define HP_SHADOW_OFFSET_Y 0.03

TextureRegion* Tank::baseRegion = NULL;
TextureRegion* Tank::turretRegion = NULL;

Tank::Tank(void)
{
}


Tank::~Tank(void)
{
}

Tank::Tank(double x, double y, double width, double height, double rotation, int maxLife, bool solid, double speed, CollisionGrid* grid)
	: DynamicGameObject( x, y, width,  height,  rotation, createBaseRegion(),  maxLife,  solid, speed)
{
	
	
	hp_font.init("arialbd.TTF", 14);					    //Build the freetype font

	if(turretRegion == NULL)
	{
		turretRegion = new TextureRegion("tank_turret.png", 0, 0, 256, 256);
	}

	gunTurret = Turret(x, y, width * 2, height * 2, rotation, turretRegion, maxLife, false, 0);

	firedBullets = vector<Bullet*>();
	timeSinceFired = 0;
	collisionId = TANK;
	ignoreCollisionsWith = TRAP | MINE | EXPLOSION | REPAIR; 

	this->grid = grid;
	fireRate = RELOAD_TIME;
	targetTurretDirection = rotation;

	this->setBoundingBox(20, 6, 88, 117); //co-ords from original image
	gunTurret.setBoundingBox(106, 51, 48, 105);
	this->speed = 0;
	playTurretTurnSound = false;
	lastMovedDistance = 0;

	fire_sound = SoundEffect("fire.wav");
	fire_sound.setGlobalVolume(GLOBAL_TANK_VOLUME);
	fire_sound.SetVolume(BULLET_VOLUME);
	
	drive_sound = SoundEffect("tank.wav");	
	driveSoundEnabled = true;
	drive_sound.setGlobalVolume(GLOBAL_TANK_VOLUME);

	crash_sound = SoundEffect("tank_smash.wav");
	//crash_sound.setGlobalVolume(GLOBAL_TANK_VOLUME);
	crash_sound.SetVolume( MAX_SPEED_VOLUME);
	

	//too much trouble temp disabled
	//turret_sound = SoundEffect("turret.wav");
	//turret_sound.setGlobalVolume(GLOBAL_TANK_VOLUME);
	//turret_sound.SetVolume( TURRET_VOLUME);
	
	//this->setRotation(-135);
}

TextureRegion* Tank::createBaseRegion()
{
	if(baseRegion == NULL)
	{
		baseRegion = new TextureRegion("tank_body.png", 0, 0, 128, 128);
	}
	return baseRegion;
}

void Tank::rotateTurret(double rotation)
{
	targetTurretDirection += rotation;
}

bool Tank::fire()
{
	return this->fire(MAX_DISTANCE);
}

bool Tank::fire(double distanceLimit)
{
	bool wasFired = false;
	if(timeSinceFired > fireRate && currentLife > 0){
		//PlaySound(NULL, 0, 0);//stop any existing fire sound
		//PlaySound(TEXT("fire.wav"), NULL, SND_FILENAME | SND_ASYNC);
		wasFired = true;
		if(distanceToCamera < SOUND_RANGE)
		{
			//on screen (technically not but will do for now)
			double vol = 1 - (distanceToCamera / SOUND_RANGE);
			fire_sound.SetVolume(vol);
			fire_sound.Play();
		}
		

		timeSinceFired = 0;
		double rot = this->gunTurret.getRotation();

		Vector2 bulletPosition = this->gunTurret.getPosition();
		double currentX = bulletPosition.x;
		double currentY = bulletPosition.y;
		double distance = (height / 2) + BULLET_SIZE ; 


		double rad = rot * TO_RADIANS;
		double distanceX = sin(rad) * distance;
		double distanceY = cos(rad) * distance;
		bulletPosition.add(distanceX, distanceY);

		double direcRot = rot + (0) ;
		int speed = this->speed;
		if(speed < 0)
		{
			//encase moving backwards dont fire backwards
			speed = 0;
		}
		double fireDistance = (distanceLimit) - distance;
		if(fireDistance > MAX_DISTANCE)
		{
			fireDistance = MAX_DISTANCE;
		}

		Bullet* newBullet = new Bullet(bulletPosition.x, bulletPosition.y, BULLET_SIZE, BULLET_SIZE, direcRot, 1, BULLET_SPEED, fireDistance, &firedBullets, grid); // - distance to adjust for alrady traveled offset
		//Bullet* newBullet = new Bullet(bulletPosition.x, bulletPosition.y, 1, 1, direcRot, 1, 1.1, 2000, firedBullets, grid); // debug bullet
		firedBullets.push_back(newBullet);
		grid->registerObject(newBullet);
	}
	return wasFired;
}




//override functions
void Tank::performCollision(GameObject* otherObject, float delta)
 {
	

	 if(otherObject->collisionId != BULLET && otherObject->collisionId != MINE)
	 {
		 directionToObject = Vector2::getAngle(otherObject->getPosition(), this->getPosition());


		//play crash sound (based on speed of fastest)
		//decide on volume based on speed
		 double thisAbsSpeed = this->speed < 0 ? this->speed * -1: this->speed;
		 double otherAbsSpeed = otherObject->speed < 0 ? otherObject->speed * -1: otherObject->speed;

		double volPercent = thisAbsSpeed > otherAbsSpeed ? thisAbsSpeed / maxSpeed: otherAbsSpeed / otherObject->maxSpeed;
		volPercent = volPercent < 0 ? volPercent * -1: volPercent; //if negative make positive

		//play sound if not playing
		if(!crash_sound.isPlaying() && volPercent > MIN_CRASH_VOLUME && thisAbsSpeed > otherAbsSpeed )
		{			
			//now do make only loudest play sound if above min threshold and not already playing

			
			if(distanceToCamera < SOUND_RANGE)
			{
				//on screen (technically not but will do for now)
				double distanceVol = 1 - (distanceToCamera / SOUND_RANGE);
				crash_sound.SetVolume(volPercent * MAX_SPEED_VOLUME * distanceVol);
				crash_sound.Play();
				SoundEffect::Update();//update sounds	
			}
					
		}

		 DynamicGameObject::performCollision(otherObject, delta);

		//
		//// reverse any turns just made
		//this->rotate(-lastTurnValue);
		//speed *= -1; //reverse direction
		//this->update(delta); //manually call update which moves one frame
		//speed *= -1; //reset direction
		//speed = 0;

		//if(speed > STOP_THRESHOLD || speed < -STOP_THRESHOLD || lastMovedDistance > 0)
		//{
		//	//handle forward/reverse collision

		//	//double directionFromObject = Vector2::getAngle(this->getPosition(), otherObject->getPosition());
		//	//double absoluteSpeed = this->speed < 0 ? this->speed * -1: this->speed; // if negative set postive
		//	//long double distance = absoluteSpeed * delta;

		//
		//	////move to last position based on direction to object
		//	//Vector2 currentPosition = this->getPosition();
		//	//double currentX = currentPosition.x;
		//	//double currentY = currentPosition.y;
		//	//

		//	//long double rad = directionToObject * TO_RADIANS; //+180 reverses direction moving aka move away from object
		//	//if(directionToObject < 0)
		//	//{
		//	//	//negative so add 180 to keep in -180 to 180 range
		//	//	rad += 180 * TO_RADIANS;
		//	//}
		//	//else
		//	//{
		//	//	rad -= 180 * TO_RADIANS;
		//	//}
		//	//long double distanceX = sin(rad) * distance;
		//	//long double distanceY = cos(rad) * distance;
		//
		//	//this->addPosition(distanceX, distanceY);

		//	if(!SAT2D(&this->boundingBox, &otherObject->boundingBox))
		//	{
		//		//still touching try reversing direction 
		//		
		//		//try turning oposite direction

		//	}
		//	
		//}
	}
 }

void Tank::draw()
{
	if(currentLife > 0)
	{
		this->DynamicGameObject::GameObject::draw();
		gunTurret.draw();

		//draw bullets that are alive
		//vector<Bullet*>::iterator it = this->firedBullets.begin();

		//while(it != this->firedBullets.end())
		//{
		//	Bullet* testBullet = *it;
		//	if(!testBullet->alive)
		//	{
		//		//bullet died so remove from list
		//		grid->unregisterObject(testBullet);
		//		//testBullet->explosion.unload();
		//		//it = this->firedBullets.erase(it);
		//		it++;
		//	}else{
		//		testBullet->draw();
		//		it++;
		//	}		
		//}
		drawHealthBar();
	}
}

void Tank::drawBullets()
{
	if(currentLife > 0)
	{

		//draw bullets that are alive
		vector<Bullet*>::iterator it = this->firedBullets.begin();

		while(it != this->firedBullets.end())
		{
			Bullet* testBullet = *it;
			if(!testBullet->alive)
			{
				//bullet died so remove from list
				grid->unregisterObject(testBullet);
				//testBullet->explosion.unload();
				//it = this->firedBullets.erase(it);
				it++;
			}else{
				testBullet->draw();
				it++;
			}		
		}
	}
}


void Tank::drawHealthBar()
{
	//now draw health bar
	
	glPushMatrix();
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
		glBegin(GL_QUADS);

			//draw all red bar, then overlay health
			glColor4f(1, 0, 0, 0);

			glVertex2f(this->getPosition().x - this->width / 2, this->getPosition().y - (this->height * 4) / 6);

			glVertex2f(this->getPosition().x + this->width / 2, this->getPosition().y - (this->height * 4) / 6);

			glVertex2f(this->getPosition().x + this->width / 2, this->getPosition().y - this->height);

			glVertex2f(this->getPosition().x - this->width / 2, this->getPosition().y - this->height);

			glColor4f(1, 1, 1, 1);
		glEnd();
				
	glPopMatrix();

	
	//calc width of health bar
	double w =  (this->width / maxLife) * currentLife;

	glPushMatrix();
		glBegin(GL_QUADS);

			//draw all red bar, then overlay health
			glColor4f(0, 1, 0, 0);

			glVertex2f(this->getPosition().x - this->width / 2, this->getPosition().y - (this->height * 4) / 6);

			glVertex2f(this->getPosition().x - this->width / 2 + w, this->getPosition().y - (this->height * 4) / 6);

			glVertex2f(this->getPosition().x - this->width / 2 + w , this->getPosition().y - this->height);

			glVertex2f(this->getPosition().x - this->width / 2, this->getPosition().y - this->height);

			glColor4f(1, 1, 1, 1);
		glEnd();
				
	glPopMatrix();
	
	
}

void Tank::drawHealthNumbers()
{
	double scaleX= GameActivity::screenWidth / (VIEW_SIZE * GameActivity::aspectRatio);
	double tPos = this->getPosition().x - (this->width * 0.4 );
	double rTPos = tPos * scaleX;

	double camX = GameActivity::camX;
	double rCamX = camX * scaleX;


	double posX =  (GameActivity::screenWidth / 2) + rTPos - rCamX;


	double scaleY= GameActivity::screenHeight / (VIEW_SIZE);
	double tPosy = this->getPosition().y - (this->height * 0.95);
	double rTPosy = tPosy * scaleX;

	double camY = GameActivity::camY;
	double rCamY = camY * scaleY;

	double posY =  (GameActivity::screenHeight / 2) + rTPosy - rCamY;
	
	//shadow
	glColor3f(0.0, 0.0, 0.0);
	print(GameActivity::hp_font, posX - (HP_SHADOW_OFFSET_X * scaleX), posY - (HP_SHADOW_OFFSET_Y * scaleY), "%d/%d", currentLife, maxLife);

	//main text
	glColor3f(1.0, 1.0, 1.0);
	print(GameActivity::hp_font, posX, posY, "%d/%d", currentLife, maxLife);
}

void Tank::setTurretRotation(double rotation)
{
	//gunTurret.setRotation(rotation);
	targetTurretDirection = rotation;
}

void Tank::rotate(double angle)
{
	this->DynamicGameObject::rotate(angle);
	gunTurret.rotate(angle);
	targetTurretDirection += angle;
	
}

void Tank::setRotation(double angle)
{
	
	this->DynamicGameObject::setRotation(angle);
	gunTurret.setRotation(angle);

}


//Overriden from DynamicGameObject
void Tank::update(double delta)
{
	distanceToCamera = Vector2::distance(this->getPosition(), Vector2(GameActivity::camX, GameActivity::camY));
	this->DynamicGameObject::update(delta);
	gunTurret.setPosition(this->getPosition().x, this->getPosition().y);
	gunTurret.update(delta);
	//SoundEffect::Update();

	for(unsigned int i = 0; i < firedBullets.size(); i++)
	{
		firedBullets[i]->update(delta);
	}
	timeSinceFired += delta;

	//if(timeSinceFired > 0.05)
	//{
	//	//remove immunity to bullets after 0.05 seconds
	//	ignoreCollisionsWith = 0;
	//}


	//handle moving sound
	if(driveSoundEnabled)
	{
		if(speed > STOP_THRESHOLD || speed < -STOP_THRESHOLD)
		{
			//decide on volume based on speed
			double volPercent = speed / maxSpeed;
			volPercent = volPercent < 0 ? volPercent * -1: volPercent; //if negative make positive
			
			lastMovedDistance = speed * delta;
			
			//play sound if not playing
			if(!drive_sound.isPlaying())
			{	
				
				if(distanceToCamera < SOUND_RANGE)
				{
					//on screen (technically not but will do for now)
					double distanceVol = 1 - (distanceToCamera / SOUND_RANGE);
					drive_sound.SetVolume(volPercent * MAX_SPEED_VOLUME * distanceVol);
					drive_sound.Play();
				}
				drive_sound.Play();
			}
			else
			{
				//is playing adust volume
				if(distanceToCamera < SOUND_RANGE)
				{
					//on screen (technically not but will do for now)
					double distanceVol = 1 - (distanceToCamera / SOUND_RANGE);
					drive_sound.SetVolume(volPercent * MAX_SPEED_VOLUME * distanceVol);					
				}
				else
				{
					drive_sound.SetVolume(0);	
				}
			}
		}
		else
		{
			lastMovedDistance = 0;
			//stop playing if is playing
			if(drive_sound.isPlaying())
			{
				drive_sound.Stop();
			}
		}
	}


	//too much trouble temp disabled
	//if manually set to play sound play it
	//if(playTurretTurnSound && !turret_sound.isPlaying())
	//{			
	//	if(distanceToCamera < VIEW_SIZE)
	//	{
	//		//on screen (technically not but will do for now)
	//		double vol = 1 - (distanceToCamera / VIEW_SIZE);
	//		turret_sound.SetVolume(vol);
	//		turret_sound.Play();
	//	}
	//}
	//else
	//{
	//	if(distanceToCamera < VIEW_SIZE)
	//	{
	//		//on screen (technically not but will do for now)
	//		double vol = 1 - (distanceToCamera / VIEW_SIZE);
	//		turret_sound.SetVolume(vol);
	//	}
	//	else
	//	{
	//		turret_sound.SetVolume(0);
	//	}
	//}

	double rot = gunTurret.getRotation() + 180;
	double targ = targetTurretDirection + 180;
	double dif = rot - targ;
	dif = (int)dif % 360 ; 
	if(turretNeedsRotating(dif))	
	{

		//too much trouble temp disabled
		//double absDiff = dif < 0 ? dif * -1: dif;
		//
		////play sound if not playing
		//if(!turret_sound.isPlaying() && absDiff > TURRET_TURN_SOUND_THRESHOLD)
		//{				
		//	
		//	if(distanceToCamera < VIEW_SIZE)
		//	{
		//		//on screen (technically not but will do for now)
		//		double vol = 1 - (distanceToCamera / VIEW_SIZE);
		//		turret_sound.SetVolume(vol);
		//		turret_sound.Play();
		//	}
		//}

		if(dif < 0)
		{
			if(dif > -180 )
			{
				this->gunTurret.rotate(delta * TURRET_TURN_SPEED); // if negative difference go clockwise
			}
			else
			{
				this->gunTurret.rotate(-delta * TURRET_TURN_SPEED); //  if positive difference go anti-clockwise
			}
		}
		else
		{
			if(dif > 180 )
			{
				this->gunTurret.rotate(delta * TURRET_TURN_SPEED); // if negative difference go clockwise
			}
			else
			{
				this->gunTurret.rotate(-delta * TURRET_TURN_SPEED); //  if positive difference go anti-clockwise
			}
		}
	}

	//too much trouble temp disabled
	//else
	//{
	//	//stop playing if is playing
	//	if(turret_sound.isPlaying() && !playTurretTurnSound)
	//	{
	//		turret_sound.Stop();
	//	}
	//}
}

bool Tank::turretNeedsRotating()
{
	double rot = gunTurret.getRotation() + 180;
	double targ = targetTurretDirection + 180;
	double dif = rot - targ;
	dif = (int)dif % 360; 
	return this->turretNeedsRotating(dif);
}

bool Tank::turretNeedsRotating(double angleDifference)
{
	if(angleDifference > TURRET_THRESHOLD || -angleDifference > TURRET_THRESHOLD)	
	{
		return true;
	}
	return false;
}


void Tank::stop()
{
	this->DynamicGameObject::stop();
	gunTurret.stop();
}


void Tank::playTurretSound()
{
	playTurretTurnSound = true;
}

void Tank::stopTurretSound()
{
	playTurretTurnSound = false;
	//too much trouble temp disabled
	/*if(turret_sound.isPlaying())
	{
		turret_sound.Stop();
	}*/
}

void Tank::stopAllSounds()
{
	//if(fire_sound.isPlaying())
	//{
		fire_sound.Stop();
	//}
	//if(crash_sound.isPlaying())
	//{
		crash_sound.Stop();
	//}
	//if(drive_sound.isPlaying())
	//{
		drive_sound.Stop();
	//}
	//if(turret_sound.isPlaying())
	//{
		//turret_sound.Stop(); //too much trouble temp disabled
	//}
}

//
//void Tank::draw()
//{
//	
//
//	//if(solid)
//	//	{
//	//		glColor4f(1, 0, 0, 0);//solids red non solids blue
//	//	}
//	//	else
//	//	{
//	//		glColor4f(0, 0, 1, 0);
//	//	}
//	//	/*
//	//	filled rectangle version
//	//	glVertex3f(this->boundingBox.vertices[0].x, this->boundingBox.vertices[0].y, 0);
//
//	//	glVertex3f(this->boundingBox.vertices[1].x, this->boundingBox.vertices[1].y, 0);
//
//	//	glVertex3f(this->boundingBox.vertices[2].x, this->boundingBox.vertices[2].y, 0);
//
//	//	glVertex3f(this->boundingBox.vertices[3].x, this->boundingBox.vertices[3].y, 0);
//	//	*/
//	//	glVertex2d(this->boundingBox.vertices[0].x, this->boundingBox.vertices[0].y);        //top left to     
//	//	glVertex2d( this->boundingBox.vertices[1].x, this->boundingBox.vertices[1].y);       //top right    
//
//	//	glVertex2d( this->boundingBox.vertices[1].x, this->boundingBox.vertices[1].y);       //top right to  
//	//	glVertex2d( this->boundingBox.vertices[2].x, this->boundingBox.vertices[2].y);       //bottom left
//
//	//	glVertex2d( this->boundingBox.vertices[2].x, this->boundingBox.vertices[2].y);       //bottom left to
//	//	glVertex2d(this->boundingBox.vertices[3].x, this->boundingBox.vertices[3].y);		//bottom right
//
//	//	glVertex2d(this->boundingBox.vertices[3].x, this->boundingBox.vertices[3].y);		//bottom right to
//	//	glVertex2d(this->boundingBox.vertices[0].x, this->boundingBox.vertices[0].y);       //top left      
//
//	//	glColor4f(1, 1, 1, 1);
//	//glEnd();
//	//			
//	//glPopMatrix();		
//
//		
//}