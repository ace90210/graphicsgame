#include "AssetManager.h"

#include <vector>

AssetManager::AssetManager(void)
{
}

AnimatedTexture AssetManager::loadAssets()
{
	Texture* atlas = new Texture("anitest.png");
	std::vector<TextureRegion> aniRegions;
	aniRegions.push_back(TextureRegion(atlas, 0, 0, 229, 283));
	aniRegions.push_back(TextureRegion(atlas, 228, 0, 229, 283));
	aniRegions.push_back(TextureRegion(atlas, 469, 0, 229, 283));
	aniRegions.push_back(TextureRegion(atlas, 697, 0, 229, 283));
	return AnimatedTexture(aniRegions, 1 / 2);
}


AssetManager::~AssetManager(void)
{
}
