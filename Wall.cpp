#include "Wall.h"
TextureRegion* Wall::wallRegion = NULL;

Wall::Wall(void)
{
}

Wall::Wall(double x, double y, double width, double height, double rotation)
	: GameObject( x, y, width, height, rotation,  createWallRegion(), 1, true)
{
	this->collisionId = WALL;
	invincible = true;
}

Wall::~Wall(void)
{
}

TextureRegion* Wall::createWallRegion()
{
	if(wallRegion == NULL)
	{
		wallRegion = new TextureRegion("wallW.png", 0, 0, 1024, 512);
	}
	return wallRegion;
}
void Wall::performCollision(GameObject* otherObject, float delta)
 {
	 
 }
