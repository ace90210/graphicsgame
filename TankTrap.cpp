#include "TankTrap.h"

#define MAX_TRAVEL_SPEED 1
#define MIN_HEALTH 5

TextureRegion* TankTrap::trapRegion = NULL;


TankTrap::TankTrap(void)
{
}


TankTrap::~TankTrap(void)
{
}

TankTrap::TankTrap (double x, double y, double width, double height, double rotation)
	: GameObject( x, y, width, height, rotation, createTrapRegion(), MIN_HEALTH, true)//TextureRegion("bullet.png", 0, 0, 32, 32)
{
	collisionId = TRAP; // 8 is or traps
	collidesWith = BULLET | TANK | EXPLOSION; //only collides with tanks or bullets

	this->setBoundingBox(0, 20, 128, 87); //co-ords from original image
}


TextureRegion* TankTrap::createTrapRegion()
{
	if(trapRegion == NULL)
	{
		trapRegion = new TextureRegion("tank_trap.png");
	}
	return trapRegion;
}


void TankTrap::performCollision(GameObject* otherObject, float delta)
{
	if(otherObject->collisionId != BULLET)
	{
		if(otherObject->speed > MAX_TRAVEL_SPEED)
		{
			otherObject->speed = MAX_TRAVEL_SPEED; //if going faster than trap speed limit slow to trap speed limit
		}
		else if(otherObject->speed < -MAX_TRAVEL_SPEED)
		{
			otherObject->speed = -MAX_TRAVEL_SPEED; //if going slower than trap negative speed limit slow to negative trap speed limit
		}
	}

	if(currentLife < 1)
	{
		solid = false;
	}
}