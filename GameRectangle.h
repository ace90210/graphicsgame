#pragma once
#include "Vector2.h"

class GameRectangle
{
public:
	Vector2 topLeft;
	Vector2 topRight;
	Vector2 bottomLeft;
	Vector2 bottomRight;


	GameRectangle(void);
	~GameRectangle(void);
	GameRectangle(Vector2 topLeft, Vector2 topRight, Vector2 bottomLeft, Vector2 bottomRight);

	void rotate(double angle);
};

