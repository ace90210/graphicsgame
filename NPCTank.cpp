#include <stdio.h>
#include <time.h>      
#include "GameActivity.h"

#include "NPCTank.h"

#define MAX_RANGE 50
#define RANGE 20
#define TANK_ROTATION_THRESHOLD 15
#define TANK_TURN_SPEED 50
#define MIN_RANGE 10
#define ACCELERATION 10
#define REVERSE_TIME 0.2
#define FORWARD_TIME 1
#define TURN_AMOUNT 45

#define SCORE_ON_HIT 100
#define SCORE_ON_DEATH 1250

NPCTank::NPCTank(void)
{
}


NPCTank::~NPCTank(void)
{
}

NPCTank::NPCTank(double x, double y, double rotation, int health, double speed, double fireRate, CollisionGrid* grid, Tank* player)
	: Tank( x, y, player->width, player->height, rotation, health, true, speed, grid)
{

	this->player = player;
	this->fireRate = fireRate;

	/* initialize random seed: */
	srand (time(NULL));
	randomDelay = genRandDouble(0, 0.75);
	targetPlayerDistance = 999999; //default high so does not start chasing by default
	
	doAvoidanceProcedure = false;
	reversingTime = 0;
	forwardTime = 0;
	avoidingTurnProgress= 0;
	altTurnDirection = false;
	seenPlayer = false;

}

void NPCTank::update(double delta)
{
	if(this->isAlive())
	{
		Tank::update(delta);
		targetPlayerDistance = Vector2::distance(player->getPosition(), this->getPosition());
		if(targetPlayerDistance > MAX_RANGE)
		{
			seenPlayer = false;
		}
		else
		{
			if(seenPlayer)
			{
				GameActivity::addSeenByTank();
			}
			else if(targetPlayerDistance < RANGE)
			{
				GameActivity::addSeenByTank();
			}
		}

		//always fire when suiable
		if(!turretNeedsRotating() && timeSinceFired > fireRate + randomDelay && currentLife > 0 && targetPlayerDistance <= MIN_RANGE)
		{		
			randomDelay = genRandDouble(0, 0.2); 
			this->fire(targetPlayerDistance); // for now test using firing
		
			if(timeSinceFired > 0.1)
			{
				//remove immunity to bullets after 0.1 seconds
				ignoreCollisionsWith = 0;
			}
		}


		//are we reversing from a collision
		if(doAvoidanceProcedure)
		{
			if(reversingTime > REVERSE_TIME)
			{
				//if not done turned enough turn more
				if(avoidingTurnProgress < TURN_AMOUNT)
				{
					//check turn direction
					avoidingTurnProgress += TANK_TURN_SPEED * delta;
					if(altTurnDirection)
					{
						//if chosen alternative direction go left
						this->rotate(delta * -TANK_TURN_SPEED); 
						//lastTurnValue = delta * -TANK_TURN_SPEED;
					}
					else
					{
						this->rotate(delta * TANK_TURN_SPEED); 
						//lastTurnValue = delta * TANK_TURN_SPEED;
					}
					forwardTime = 0; 
				}
				else
				{
					//done turning move forward set distance

					if(forwardTime < FORWARD_TIME)
					{
						this->increaseSpeed(ACCELERATION * delta); //reverse up
						forwardTime += delta;
					}
					else
					{
						reversingTime = 0;
						avoidingTurnProgress = 0;
						forwardTime = 0;
						doAvoidanceProcedure = false; //assume we are free until collision grid redetects collision
					}
				}
		
			}
			else
			{
				this->decreaseSpeed(ACCELERATION * delta); //reverse up
				reversingTime += delta;
			
			}
		}
		else
		{
			//normal state, (not avoiding things)
			if((targetPlayerDistance < RANGE || seenPlayer) && player->currentLife > 0)
			{

				seenPlayer = true;
				targetPlayerDirection = Vector2::getAngle(player->getPosition(), this->getPosition());
				this->setTurretRotation(targetPlayerDirection);
		
		
			
				double rot = this->getRotation() + 180;
				double targ = targetPlayerDirection + 180;
				double dif = rot - targ;

				dif = (int)dif % 360 ; 
				if(tankNeedsRotating(dif))	
				{
					if(dif < 0)
					{
						if(dif > -180 )
						{
							this->setTurn(TANK_TURN_SPEED); // if negative difference go clockwise
							//lastTurnValue = delta * TANK_TURN_SPEED;
						}
						else
						{
							this->setTurn(-TANK_TURN_SPEED); //  if positive difference go anti-clockwise
							//lastTurnValue = -delta * TANK_TURN_SPEED;
						}
					}
					else{
						if(dif > 180 )
						{
							this->setTurn(TANK_TURN_SPEED); // if negative difference go clockwise
							//lastTurnValue = delta * TANK_TURN_SPEED;
						}
						else
						{
							this->setTurn(-TANK_TURN_SPEED); //  if positive difference go anti-clockwise
							//lastTurnValue = -delta * TANK_TURN_SPEED;
						}
					}
					if(speed > STOP_THRESHOLD)
					{
						this->decreaseSpeed(ACCELERATION * delta);
					}
					else if(speed < -STOP_THRESHOLD)
					{
						this->increaseSpeed(ACCELERATION * delta);
					}
				}
				else if(targetPlayerDistance > MIN_RANGE)
				{
					this->stopTurn();
					//if facing right way and not too close move closer
					this->increaseSpeed(ACCELERATION * delta);
				}
				else if(speed > STOP_THRESHOLD)
				{
					this->stopTurn();
					//slow down we are close enough
					this->decreaseSpeed(ACCELERATION * delta);
				}
				else
				{
					this->stopTurn();
				}
			}
			else if(speed > STOP_THRESHOLD)
			{
				this->stopTurn();
				//if moving stop moving			
				this->decreaseSpeed(ACCELERATION * delta);
			}
			else if(speed < -STOP_THRESHOLD)
			{
				this->stopTurn();
				//if moving stop moving	
				this->increaseSpeed(ACCELERATION * delta);
			}
		}
	}
}

bool NPCTank::tankNeedsRotating(double angleDifference)
{
	if(angleDifference > TANK_ROTATION_THRESHOLD || -angleDifference > TANK_ROTATION_THRESHOLD)	
	{
		return true;
	}
	return false;
}

double NPCTank::genRandDouble(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

void NPCTank::performCollision(GameObject* otherObject, float delta)
 {
	 
 	 if(currentLife <= 1 && otherObject->collisionId == BULLET) 
	 {
		GameActivity::addToScore(SCORE_ON_DEATH);
	 }
	 else if(otherObject->collisionId == BULLET)
	 {
		 GameActivity::addToScore(SCORE_ON_HIT);
		 if(!seenPlayer)
		 {
			 //if not seen player you have now (if in max range)
			 seenPlayer = true;
		 }
	 }

	 //only consider avoidance when not being hit by bullet or explosion
	if((speed != 0 || rotationalVelocity != 0) && otherObject->collisionId != EXPLOSION && otherObject->collisionId != BULLET)
	{
			//handle forward/reverse collision
			 directionToObject = Vector2::getAngle(otherObject->getPosition(), this->getPosition());
			//set to 360 for ease of understanding
			directionToObject += 180;

			double rotation = this->getRotation() + 180;
			if( rotation - 90 < directionToObject && rotation + 90 > directionToObject)
			{
				//object is infront
				//if object infront perform collision moves(flag as doAvoidanceProcedure) else object behind so dont try anything
		
				reversingTime = 0; //reset avoidance details
				forwardTime = 0;
				avoidingTurnProgress= 0;
				if(!doAvoidanceProcedure)
				{
					if(rotationalVelocity > 0)
					 {
						 doAvoidanceProcedure = true;
						 altTurnDirection = false; // going clockwise so go anti clockwise
						 reversingTime = 0; //reset avoidance details
						forwardTime = 0;
						avoidingTurnProgress= 0;
					 }
					 else if(rotationalVelocity < 0)
					 {
						  doAvoidanceProcedure = true;
						 altTurnDirection = true; //going anti clockwise so go clockwise
						 reversingTime = 0; //reset avoidance details
						forwardTime = 0;
						avoidingTurnProgress= 0;
					 }
					else
					{
						//if not in middle of avoidance precedure pick random direction (in in middle keep trying same direction (aka dont hcange altTurn direction from current value)
						int randTurnDirection = rand() % 2;		
						altTurnDirection = randTurnDirection == 0; // decide if to do alternate turn direction
					 }
				}
		
		
				doAvoidanceProcedure = true;
			}
			else // && rotation >= directionToObject + 90 && rotation <= directionToObject + 270)
			{
				//if object behind and was reversing so stop the avoidance precedure

				doAvoidanceProcedure = false;
			
			}
	 }
	//else
	//{
	//	//wasnt moving forward must have been turning so do avoidance anyway
	//	doAvoidanceProcedure = true;
	//	reversingTime = 0; //reset avoidance details
	//	forwardTime = 0;
	//	avoidingTurnProgress= 0;

	//	if(doAvoidanceProcedure)
	//	{
	//		//if not in middle of avoidance precedure try other way
	//		altTurnDirection = !altTurnDirection;
	//	}
	//}

	Tank::performCollision(otherObject, delta);
 }