#include "Texture.h"


Texture::Texture(std::string texture)
{
	
	GLint w, h;
	glEnable(GL_TEXTURE_2D);
	textureId = SOIL_load_OGL_texture(texture.c_str(),		// filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);				// generate Mipmaps and invert Y
	glBindTexture(GL_TEXTURE_2D, textureId);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &h);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &w);
	this->width = w;
	this->height = h;
}

Texture::Texture(void)
{
}

Texture::~Texture(void)
{
}
