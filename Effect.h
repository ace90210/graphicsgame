#pragma once

enum EFFECT { FADE, TWO_WAY_FADE };
class Effect
{
private:
		EFFECT mode;
	float currentTimeIndex, pauseDuration, fullduration, range;
	bool loop, completed;

	//two way variables
	float mid;

	void fade();
	void twoWayFade();

	float interporlation(float start, float end, float a);

public:
	bool active;
	float  duration, startValue, endValue, currentValue;

	Effect(EFFECT mode, float duration, float startValue, float endValue, bool loop);
	Effect(EFFECT mode, float duration, float pauseDuration, float startValue, float endValue, bool loop);
	Effect(void);
	~Effect(void);

	bool update(float delta);

	void reset(bool loop);

	void reset();

	 bool isComplete();


	 float getDuration();
	 void setDuration(float duration);
};

