#pragma once
#include "DynamicGameObject.h"
class Turret: public DynamicGameObject
{
public:
	Turret(double x, double y, double width, double height, double rotation, TextureRegion* textureRegion, int maxLife, bool solid, double speed);
	Turret(void);
	~Turret(void);

	void performCollision(GameObject* otherObject, float delta);
};

