#include "Effect.h"


Effect::Effect(void)
{
}


Effect::~Effect(void)
{
}


Effect::Effect(EFFECT mode, float duration, float startValue, float endValue, bool loop) {	
	this->mode = mode;
	this->duration = duration;
	this->fullduration = duration;
	this->startValue = startValue;
	this->endValue = endValue;
	this->pauseDuration = 0;
	reset(loop);
}

	Effect::Effect(EFFECT mode, float duration, float pauseDuration, float startValue, float endValue, bool loop) {	
		this->mode = mode;
		this->duration = duration;
		this->fullduration = duration + pauseDuration;
		this->startValue = startValue;
		this->endValue = endValue;
		this->pauseDuration = pauseDuration;
		reset(loop);
	}
	
	bool Effect::update(float delta){
		if(active){
			currentTimeIndex += delta;
			switch(mode){
				case FADE: fade(); break;
				case TWO_WAY_FADE:  twoWayFade();
			}
		}
		return active;
	}
	
	void Effect::fade(){
		if(currentTimeIndex > fullduration){
			currentValue = endValue;
			currentTimeIndex = 0;
			if(!loop){
				active = false;
				completed = true;
				return;
			}				
		}
		if(currentTimeIndex > pauseDuration){
			float percentDone = ((currentTimeIndex - pauseDuration) / duration);
			currentValue = interporlation(currentValue, startValue + (range*percentDone), 0.1f);
		}
	}
	
	void Effect::twoWayFade(){
		if(currentTimeIndex > fullduration){
			currentValue = startValue;
			currentTimeIndex = 0;
			if(!loop){
				active = false;
				completed = true;
				return;
			}				
		}
		if(currentTimeIndex < mid){			
			//first half
			float percentDone = (currentTimeIndex / mid);
			currentValue = interporlation(currentValue, startValue + (range*percentDone), 0.1f);
		}else{
			if(currentTimeIndex > mid + pauseDuration){
				//second half
				float percentDone = ((currentTimeIndex - pauseDuration) / mid);
				percentDone = 2 - percentDone;
				currentValue = interporlation(currentValue, startValue + (range*percentDone), 0.1f);
			}
		}
		
	}
	
	void Effect::reset(bool loop){
		this->currentTimeIndex = 0;
		this->currentValue = 0;
		this->active = true;
		this->loop = loop;
		range = endValue - startValue;
		mid = duration / 2;
		completed = false;
	}
	
	void Effect::reset(){
		this->currentTimeIndex = 0;
		this->currentValue = 0;
		this->active = true;
		this->loop = false;
		range = endValue - startValue;
		mid = duration / 2;
		completed = false;
	}
	
	bool Effect::isComplete(){
		if(completed && !loop){
			return true;
		}
		return false;
	}

	float Effect::interporlation(float start, float end, float a)
	{
		return start + (end - start) * a;
	}