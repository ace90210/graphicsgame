#include "TileEntity.h"


TileEntity::TileEntity(void)
{
	this->itemId = 0;
	this->layer = 0; 
	this->health = 1;
	this->width = 1;
	this->height = 1;
	this->speed = 0;
	this->rotation = 0;
	this->fireRate = 1;
	this->absoluteSize = false;
}

TileEntity::TileEntity(int id)
{
	this->itemId = id;
	this->layer = 0; 
	this->health = 1;
	this->width = 1;
	this->height = 1;
	this->speed = 0;
	this->rotation = 0;
	this->fireRate = 1;
	this->absoluteSize = false;
}

TileEntity::TileEntity(int id, int layer, int health, double width, double height, bool absoluteSize)
{
	this->itemId = id;
	this->layer = layer; 
	this->health = health;
	this->width = width;
	this->height = height;	
	this->absoluteSize = absoluteSize;
	this->speed = 0;
	this->rotation = 0;
	this->fireRate = 1;

}

TileEntity::TileEntity(int id, int layer, int health, double fireRate, double width, double height, double speed, double rotation, bool absoluteSize)
{
	this->itemId = id;
	this->layer = layer; 	
	this->health = health;
	this->width = width;
	this->height = height;
	this->speed = speed;
	this->rotation = rotation;
	this->fireRate = fireRate;	
	this->absoluteSize = absoluteSize;
}

TileEntity::TileEntity(int id, int layer, int health, double width, double height, double speed, double rotation, bool absoluteSize)
{
	this->itemId = id;
	this->layer = layer; 
	this->health = health;
	this->width = width;
	this->height = height;
	this->speed = speed;
	this->rotation = rotation;
	this->absoluteSize = absoluteSize;
	this->fireRate = 1;
}

TileEntity::~TileEntity(void)
{
}
