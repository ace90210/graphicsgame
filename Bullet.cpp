#include "Bullet.h"
#include "GameActivity.h"

#define TARGET_TRAP_HIT_RADIUS 1
#define SOUND_RANGE 35
#define MIN_SOUND 0.2

TextureRegion* Bullet::bulletRegion = NULL;

Bullet::Bullet(double x, double y, double width, double height, double rotation, int maxLife, double speed, double distance, vector<Bullet*>* firedBullets, CollisionGrid* grid) 
	: DynamicGameObject( x, y, width,  height,  rotation,  createBulletRegion(),  maxLife,  true, speed)
{
	collisionId = BULLET;
	ignoreCollisionsWith = BULLET | TANK | EXPLOSION ;
	timeAlive = 0;
	distanceCovered = 0;
	distanceLimit = distance;
	startPosition = Vector2(x, y);
	this->setSpeed(speed);
	this->firedBullets = firedBullets;
	this->grid = grid;	
	alive = true;
	
	explosion = NULL;
}


Bullet::Bullet(void)
{
}


Bullet::~Bullet(void)
{
}


TextureRegion* Bullet::createBulletRegion()
{
	if(bulletRegion == NULL)
	{
		bulletRegion = new TextureRegion("bullet.png", 0, 0, 32, 32);
	}
	return bulletRegion;
}

void Bullet::update(double delta)
{
	
	timeAlive += delta;
	DynamicGameObject::update(delta);
	distanceCovered += delta * speed;
	long double dis = distanceCovered;
	
	//long double dis = Vector2::distance(Vector2(this->getPosition().x, this->getPosition().y), startPosition);

	if(timeAlive > LIFE_LIMIT || dis  >= distanceLimit)
	{
		currentLife--; // die after certain time / distance
		//deleteBullet();
	}
	else if(timeAlive > 0.05)
	{
		//remove immunity to tanks after 0.05 seconds
		ignoreCollisionsWith = BULLET | EXPLOSION;
	}

	if(currentLife < 1 && alive)
	{	
		alive = false;
		GameActivity::addExplosion(this->getPosition().x, this->getPosition().y);

		//if(explosion != NULL)
		//{
		//	if(explosion->done)
		//	{
		//		//done exploding delete this bullet
		//		this->deleteBullet();
		//		alive = false;
		//		return;
		//	}
		//	explosion->update(delta);
		//}
		//else
		//{
		//	explosion = new Nuke(this->getPosition().x, this->getPosition().y, 2, 2, 0.226667);
		//	double camDistance = Vector2::distance(this->getPosition(), Vector2(GameActivity::camX, GameActivity::camY));
		//	double vol = 1 - (camDistance / SOUND_RANGE);
		//	if(vol < MIN_SOUND)
		//	{
		//		vol = MIN_SOUND;
		//	}
		//	explosion->startPlayback(vol);
		//}
		//if(GameActivity::testNuke.done && alive)
		//{
		//	//done exploding delete this bullet
		//	this->deleteBullet();
		//	alive = false;
		//	GameActivity::testNuke.done = false;
		//	return;
		//}else if(!GameActivity::testNuke.done && !GameActivity::testNuke.play)
		//{
		//	//GameActivity::testNuke.setPosition(this->getPosition().x, this->getPosition().y);
		//	GameActivity::testNuke.startPlayback();
		//}
	}
	
}

void Bullet::performCollision(GameObject* otherObject, float delta)
 {
	 
		 if(otherObject->collisionId != TRAP || (otherObject->collisionId == TRAP && this->distanceLimit - this->distanceCovered < TARGET_TRAP_HIT_RADIUS))
		 {
			 this->currentLife--;
			 //if not invincible
			 //if(!otherObject->invincible)
			 //{
				// //if hitting a trap only trigger if within trap hit radius, this means traps can be shot "over"				 
				// otherObject->currentLife--;				 
			 //}
		 }
		 
 }



void Bullet::deleteBullet()
{
	for(int i = 0; i < firedBullets->size(); i++)
	{
		if(this == (*firedBullets)[i])
		{
			//found so remove from collision grid then fireBullets, finally delet altogether
			grid->unregisterObject(this);
			explosion->unload();
			firedBullets->erase(firedBullets->begin() + i);
			//delete this;
			return;
		}
	}
}

void Bullet::draw()
{
	if(explosion == NULL)
	{
		DynamicGameObject::draw();
	}
	else
	{
		explosion->draw();
	}
}