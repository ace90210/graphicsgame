#include "AnimatedTexture.h"

#include <iostream>

AnimatedTexture::AnimatedTexture(void)
{
}

AnimatedTexture::AnimatedTexture(std::vector<TextureRegion> regions, double duration)
{
	n = regions.size();
	keyframes = regions;
	this->duration = duration;
	timeElapsed = 0;
	this->mode = PINGPONG;
}

TextureRegion* AnimatedTexture::getKeyframe(double time)
{
	int frameId = (int)(timeElapsed / duration);

	if(mode == PINGPONG)
	{
		if(frameId == n)
		{
			//we want to skip this one
			timeElapsed += duration;
			frameId = (int)(timeElapsed / duration);
		}

		int tmp = frameId % (n * 2); // range from start to end and back ie including repeat section = 2 x n
		if(tmp >= n)
		{
			//we are over half way through the loop calculate decending frame ids
			//y = tmp - n + 1; 
			//frameId = n - y;
			frameId = (2 * n) - tmp - 1;
		}
		else
		{
			frameId = frameId % n;
		}
	}
	else
	{
		if(frameId >= n)
		{
			frameId = n - 1;
		}
	}

	// DEBUG area
	if(lastFrame != frameId)
	{
		history.push_back(frameId);
	}

	lastFrame = frameId;
	timeElapsed += time;
	return &keyframes[frameId];
}

AnimatedTexture::~AnimatedTexture(void)
{
}
