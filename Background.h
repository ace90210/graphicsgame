#pragma once
#include <string>
#include "Texture.h"

class Background: public Texture
{
public:
	double x, y;
	double width, height;
	Background(std::string textureUrl, double x, double y, double width, double height);
	Background(void);
	~Background(void);

	void draw();
};

