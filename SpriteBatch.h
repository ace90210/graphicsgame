#pragma once
#include <Windows.h>
#include <gl/gl.h>			// Header File For The OpenGL32 Library

#include "Vertices.h"
#include "TextureRegion.h"

class SpriteBatch {
private:
	float *verticesBuffer;
	int bufferIndex;
	Vertices * vertices;
	int numSprites;

public:
	SpriteBatch(int maxSprites);

	void beginBatch(GLuint textureID);

	void endBatch();

	void drawSprite(float x, float y, float width, float height, TextureRegion * region);

	void drawSprite(float x, float y, float width, float height, float angle, TextureRegion * region);
};