#include "GameRectangle.h"


GameRectangle::GameRectangle(void)
{
}


GameRectangle::~GameRectangle(void)
{
}

GameRectangle::GameRectangle(Vector2 topLeft, Vector2 topRight, Vector2 bottomLeft, Vector2 bottomRight)
{
	this->topLeft = topLeft;
	this->topRight = topRight;
	this->bottomLeft = bottomLeft;
	this->bottomRight = bottomRight;
}

void GameRectangle::rotate(double angle)
{
	topLeft.rotate(angle);
	topRight.rotate(angle);
	bottomLeft.rotate(angle);
	bottomRight.rotate(angle);
}