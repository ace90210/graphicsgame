#include "CollisionGrid.h"
#include "Collision.h"

CollisionGrid::CollisionGrid(void)
{
	objs = vector<GameObject*>();
}


CollisionGrid::~CollisionGrid(void)
{
}

void CollisionGrid::registerObject(GameObject* obj)
{
	objs.push_back(obj);
}

void CollisionGrid::unregisterObject(GameObject* obj)
{
	for(int i = 0; i < objs.size(); i++)
	{
		if(obj == objs[i])
		{
			//found so remove
			objs.erase(objs.begin() + i);
			return;
		}
	}
}

int CollisionGrid::update(float delta)
{
	int collisions = 0;
	//loop all objects
	for(int i = 0; i < objs.size(); i++)
	{
		//if this item is solid solid
		if(objs[i]->solid && objs[i]->currentLife > 0){

			//check against all other objects
			for(int j = i; j < objs.size(); j++)
			{
				//skip i == j obviously they collide with themselves
				if(i == j){
					continue;
				}
				
				if(objs[j]->currentLife <= 0)
				{
					//if item j dead skip collision
					continue;
				}

				//only both with check if other object is also solid
				if(objs[j]->solid){
					//check for if an ignored collision item, i == 1, j == b
					unsigned int colId=objs[j]->collisionId;
					unsigned int colId2=objs[i]->collisionId;
					unsigned int aIgnoreB = colId & objs[i]->ignoreCollisionsWith;
					unsigned int bIgnoreA = colId2 & objs[j]->ignoreCollisionsWith;

					if( aIgnoreB > 0 && bIgnoreA > 0) // if ignored both ways
					{
						continue; //skip
					}
					//check items collide with each other
					unsigned int doesCollide = objs[i]->collidesWith & objs[j]->collisionId;

					if(doesCollide > 0 && !SAT2D(&objs[i]->boundingBox, &objs[j]->boundingBox)){
						collisions++; 
						if( aIgnoreB == 0) // only perform a does not ignore b
						{
							objs[i]->performCollision(objs[j], delta);
						}
						if( bIgnoreA == 0) // only perform b does not ignore a
						{
							objs[j]->performCollision(objs[i], delta);
						}
					}
				}
			}
		}
	}

	return collisions;
}

vector<int> CollisionGrid::getCollisionsWith(GameObject* obj)
{
	vector<int> collidingWith = vector<int>();

	//check against all other objects
	for(int j =0; j < objs.size(); j++)
	{

		//skip if already in list
		bool inList = false;
		for(int i = 0; i < collidingWith.size(); i++)
		{
			if(objs[j]->collisionId == collidingWith[i])
			{
				inList = true;
				break;
			}
		}
		if(inList){
			continue;
		}
				
		if(objs[j]->currentLife <= 0)
		{
			//if item j dead skip collision
			continue;
		}

		//check if other object is also solid
		if(objs[j]->solid){
			//check for if an ignored collision item, i == 1, j == b
			unsigned int colId=objs[j]->collisionId;
			unsigned int colId2=obj->collisionId;

			unsigned int aIgnoreB = colId & obj->ignoreCollisionsWith;
			unsigned int bIgnoreA = colId2 & objs[j]->ignoreCollisionsWith;

			if( aIgnoreB > 0 && bIgnoreA > 0) // if ignored both ways
			{
				continue; //skip
			}

			//check items collide with each other
			unsigned int doesCollide = obj->collidesWith & objs[j]->collisionId;

			if(doesCollide > 0 && !SAT2D(&obj->boundingBox, &objs[j]->boundingBox)){
				collidingWith.push_back(colId);				
			}
		}
	}
	return collidingWith;
}