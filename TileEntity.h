#pragma once
class TileEntity
{
public:
	double width, height;
	double rotation;
	double speed;
	double fireRate;
	int health;
	int layer;
	int itemId;
	bool absoluteSize;

	TileEntity(void);
	TileEntity(int id);
	TileEntity(int id, int layer, int health, double width, double height, bool absoluteSize);
	TileEntity(int id, int layer, int health, double width, double height, double speed, double rotation, bool absoluteSize);
	TileEntity(int id, int layer, int health, double fireRate, double width, double height, double speed, double rotation, bool absoluteSize);
	
	
	~TileEntity(void);
};

