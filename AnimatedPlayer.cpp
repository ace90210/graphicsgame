#include "AnimatedPlayer.h"
#include "GameActivity.h"

#define SCORE_ON_HIT 10
#define SCORE_ON_DEATH 125

AnimatedPlayer::AnimatedPlayer(double x = 0, double y = 0, double width = 1, double height = 1, double rotation = 0, int maxLife = 1, bool solid = true, std::vector<TextureRegion> regions = std::vector<TextureRegion>(), double duration = 1)
	:  AnimatedGameObject(x, y, width, height, rotation, maxLife, solid, regions, duration)
{
	ignoreCollisionsWith = EXPLOSION;
	this->setBoundingBox(39 , 12 , 148, 272 );
}


AnimatedPlayer::AnimatedPlayer(void)
{
}


AnimatedPlayer::~AnimatedPlayer(void)
{
}


 void AnimatedPlayer::performCollision(GameObject* otherObject, float delta)
 {
	 if(currentLife <= 1 && otherObject->collisionId == 2) //2 = bullet
	 {
		GameActivity::addToScore(SCORE_ON_DEATH);
	 } else if(otherObject->collisionId == 2)
	
	 {
		 GameActivity::addToScore(SCORE_ON_HIT);
	 }
	// currentLife--;
 }