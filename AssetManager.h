#pragma once
#include "AnimatedTexture.h"

class AssetManager
{
public:
	//not working atm ... Add all game assets here, dont forget to add to load assets list
	//static AnimatedTexture testAnimation;

	AssetManager(void);
	~AssetManager(void);
	
	static AnimatedTexture loadAssets(); //load in assets as static for easy access at any point from anywhere
};

