#include "Vector2.h"
#include <math.h> 

#define M_PI 3.14159265358979323846

const double Vector2::TO_RADIANS = (1 / 180.0f) * M_PI;	
const double Vector2::TO_DEGREES = (1 / M_PI) * 180;

Vector2::Vector2(void)
{
	this->x = 0;
	this->y = 0;
}


Vector2::Vector2(double x, double y)
{
	this->x = x;
	this->y = y;
}

void Vector2::add(Vector2 v)
{
	this->x += v.x;
	this->y += v.y;
}

void Vector2::add(double x, double y)
{
	this->x += x;
	this->y += y;
}

void Vector2::subtract(Vector2 v)
{
	this->x -= v.x;
	this->y -= v.y;
}

void Vector2::subtract(double x, double y)
{
	this->x -= x;
	this->y -= y;
}

void Vector2::rotate(double angle)
{
	angle *= -1; // correction for rotating anti clockwise
	double rad = angle * TO_RADIANS;
	double cosine = cos(rad);
	double sine = sin(rad);
		
		
	double newX = this->x * cosine - this->y * sine;
	double newY = this->x * sine + this->y * cosine;
		
	this->x = newX;
	this->y = newY;

}

Vector2::~Vector2(void)
{
}

long double Vector2::distance(Vector2 v1, Vector2 v2)
{
	long double a = (v1.x - v2.x) ;

	long double b = (v1.y - v2.y);
	a *= a;
	b *= b;

	return sqrt(a + b);
	//return Q_rsqrt(a + b);
}

float Vector2::Q_rsqrt( float number )
{
	long i;
	float x2, y;
	const float threehalfs = 1.5F;
 
	x2 = number * 0.5F;
	y  = number;
	i  = * ( long * ) &y;                       // evil floating point bit level hacking
	i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
	y  = * ( float * ) &i;
	y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
//      y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed
 
	return y;
}


long double Vector2::getAngle(Vector2 v1, Vector2 v2)
{
	long double x1 = v1.x - v2.x;
	long double y1 = v1.y - v2.y;
	if(y1 != 0){
		return atan2 (x1, y1) * TO_DEGREES;
	}
	if(x1 < 0)
	{
		return -90;
	}
	else
	{
		return 90;
	}
}