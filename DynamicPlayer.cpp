#include "DynamicPlayer.h"


DynamicPlayer::DynamicPlayer(double x, double y, double width, double height, double rotation, TextureRegion* textureRegion, int maxLife, bool solid, double speed)
	: DynamicGameObject( x, y, width,  height,  rotation,  textureRegion,  maxLife,  solid, speed)
{
}

DynamicPlayer::DynamicPlayer(void)
{
}


DynamicPlayer::~DynamicPlayer(void)
{
}


void DynamicPlayer::performCollision(GameObject* otherObject, float delta)
 {
	 this->addPosition(0.0, 0);
 }