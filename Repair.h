#pragma once
#include "gameobject.h"
class Repair :
	public GameObject
{
public:	
	static TextureRegion* repairRegion;

	int repairValue;

	Repair(void);
	~Repair(void);

	Repair(int repairValue, double x, double y, double width, double height, double rotation);

	void performCollision(GameObject* otherObject, float delta);

	TextureRegion* createRepairRegion();

};

