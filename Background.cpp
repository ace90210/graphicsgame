#include "Background.h"


Background::Background(void)
{
}


Background::~Background(void)
{
}

Background::Background(std::string textureUrl, double x, double y, double width, double height) 
	: Texture(textureUrl) 
{
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
}

void Background::draw()
{
	glPushMatrix();
		glTranslated(x, y, 0.0);			// Move the player to the required position
		// Bind our player texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, textureId);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Use two triangles to make a square, with texture co-ordinates for each vertex
		glBegin(GL_TRIANGLES);
			glColor3f(1.0f, 1.0f, 1.0f);

			glTexCoord2f(0, 0);
			glVertex2f(0, 0);

			glTexCoord2f(1, 0);
			glVertex2f(width, 0);

			glTexCoord2f(0, 1);
			glVertex2f(0, height);


			glTexCoord2f(1, 0);
			glVertex2f(width, 0);

			glTexCoord2f(1, 1);
			glVertex2f(width, height);

			glTexCoord2f(0, 1);
			glVertex2f(0, height);
		glEnd();

		// Disable 2D texturing
		glDisable(GL_TEXTURE_2D);

		glPopMatrix();
}