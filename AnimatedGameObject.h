#pragma once
#include <vector>

#include "GameObject.h"
#include "AnimatedTexture.h"

class AnimatedGameObject: public GameObject
{
public:
	double time;
	AnimatedTexture texture;

	AnimatedGameObject(double x, double y, double width, double height, double rotation, int maxLife, bool solid, std::vector<TextureRegion> regions, double duration);
	AnimatedGameObject(void);
	~AnimatedGameObject(void);

	void update(float delta);
	void draw();
};

