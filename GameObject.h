#pragma once



#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library

#include <string>

#include "Vector2.h"
#include "TextureRegion.h"
#include "Shape.h"

static enum DEBUG_MODE {OFF, SOLIDS_ONLY, ALL };

const int DEFAULT = 1, BULLET = 2, TANK = 4, TRAP = 8, WALL = 16, EXPLOSION = 32, MINE = 64, REPAIR = 128;

class GameObject
{
private:
	
	Vector2 topLeft;
	Vector2 topRight;
	Vector2 bottomLeft;
	Vector2 bottomRight;
	Vector2 position;
	double oRotation;

public:
	
	Vector2 t0;
	Vector2 t1;
	Vector2 t2;
	Vector2 t3;

	unsigned int collisionId; //used as id for collision Groups, 1 = default group
	unsigned int collidesWith; //item ids this object will collide with, using flags
	unsigned int ignoreCollisionsWith; //item ids this object will NOT collide with, using flags
	TextureRegion* textureRegion;
	
	Shape boundingBox, templateBoundingBox;
	double height;
	double width;
	double speed;
	
	double maxSpeed;
	static DEBUG_MODE debug;
	double lastTurnValue;

	int maxLife;
	int currentLife;
	
	bool solid;
	bool invincible;


	GameObject(double x, double y, double width, double height, double rotation, TextureRegion* textureRegion, int maxLife, bool solid);
	GameObject(void);
	~GameObject(void);

	static void cycleDebugModes();
	static std::string getDebugName();

	bool isAlive();

	virtual void draw();

	virtual void rotate(double angle);

	virtual void setRotation(double angle);

	double getRotation();

	void setBoundingBox(double x, double y, double width, double height);

	virtual void setPosition(double x, double y);

	virtual void addPosition(double x, double y);
	
	Vector2 getPosition();

	virtual void performCollision(GameObject* otherObject, float delta) = 0;
};

