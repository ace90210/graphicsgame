#pragma once
#include "AnimatedGameObject.h"

class AnimatedPlayer: public AnimatedGameObject
{
public:

	AnimatedPlayer(double x, double y, double width, double height, double rotation, int maxLife, bool solid, std::vector<TextureRegion> regions, double duration);
	AnimatedPlayer(void);
	~AnimatedPlayer(void);


	//implemneting abstract class from inherited class
	 void performCollision(GameObject* otherObject, float delta);


};

