#pragma once

#include "Texture.h"
#include <string>


class TextureRegion 
{
public:
	Texture* texture;
	double u1, v1;
	double u2, v2;

	double x, y, width, height;

	TextureRegion(void);

	TextureRegion(std::string textureIn, double x, double y, double width, double height);

	TextureRegion(std::string textureURL);

	TextureRegion(Texture* origTexture, double x, double y, double width, double height);

	TextureRegion(Texture* origTexture, double texWidth, double textHeight, double x, double y, double width, double height);

	//virtual void setRegion(float newHeight);				// Called to change region height
};
