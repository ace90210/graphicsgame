#pragma once
#include "gameobject.h"


#define STOP_THRESHOLD  0.5

class DynamicGameObject :
	public GameObject
{
protected:
public:
	//double speed; //moved to GameObject for collision comparisions
	static const double TO_RADIANS;	
	static const float TO_DEGREES;
	double lastTurnValue;

	Vector2 velocity;

	double rotationalVelocity;
	long double distanceMovedLastFrame;

	DynamicGameObject(double x, double y, double width, double height, double rotation, TextureRegion* textureRegion, int maxLife, bool solid, double speed);
	DynamicGameObject(void);
	~DynamicGameObject(void);

	virtual void update(double delta);

	
	virtual void rotate(double angle);

	virtual void setRotation(double angle);

	
	virtual void performCollision(GameObject* otherObject, float delta);

	virtual void setSpeed(double speed);
	virtual void increaseSpeed(double acceleration);
	virtual void decreaseSpeed(double deceleration);
	virtual void stop();

	
	virtual void setTurn(double speed);
	virtual void stopTurn();

};

