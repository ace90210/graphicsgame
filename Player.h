#pragma once
#include "GameObject.h"

class Player: public GameObject
{
public:
	Player(double x, double y, double width, double height, double rotation, TextureRegion* textureRegion, int maxLife, bool solid);
	Player(void);
	~Player(void);

	void performCollision(GameObject* otherObject, float delta);
};

