#include "SpriteBatch.h"

#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library

#include <math.h>       /* cos, sin etc */

#define PI 3.14159265

SpriteBatch::SpriteBatch(int maxSprites){
	verticesBuffer = new float[maxSprites * 4 * 4];
	vertices = new Vertices(maxSprites * 4, maxSprites * 6, false, true);
	bufferIndex = 0;
	numSprites = 0;
		
	short * indices = new short[maxSprites * 6];
	int len = sizeof(indices) / sizeof(short);
	short j =0;
	for(int i = 0; i < len; i += 6, j += 4) {
		indices[i + 0] = (short)(j + 0);
		indices[i + 1] = (short)(j + 1);
		indices[i + 2] = (short)(j + 2);
		indices[i + 3] = (short)(j + 2);
		indices[i + 4] = (short)(j + 3);
		indices[i + 5] = (short)(j + 0);
	}
	vertices->setIndices(indices, 0, len);
}

void SpriteBatch::beginBatch(GLuint textureID){
	glBindTexture( GL_TEXTURE_2D, textureID );  
    numSprites = 0;                                 
    bufferIndex = 0; 
}

void SpriteBatch::endBatch(){
	vertices->setVertices(verticesBuffer, 0, bufferIndex);
	vertices->bind();
	vertices->draw(GL_TRIANGLES, 0, numSprites * 6);
	vertices->unbind();
}

void SpriteBatch::drawSprite(float x, float y, float width, float height, TextureRegion * region){
	float halfWidth = width / 2;
	float halfHeight = height / 2;
	float x1 = x - halfWidth;
	float y1 = y - halfHeight;
	float x2 = x + halfWidth;
	float y2 = y + halfHeight;
		
	verticesBuffer[bufferIndex++] = x1;
	verticesBuffer[bufferIndex++] = y1;
	verticesBuffer[bufferIndex++] = region->u1;
	verticesBuffer[bufferIndex++] = region->v2;
		
	verticesBuffer[bufferIndex++] = x2;
	verticesBuffer[bufferIndex++] = y1;
	verticesBuffer[bufferIndex++] = region->u2;
	verticesBuffer[bufferIndex++] = region->v2;
		
	verticesBuffer[bufferIndex++] = x2;
	verticesBuffer[bufferIndex++] = y2;
	verticesBuffer[bufferIndex++] = region->u2;
	verticesBuffer[bufferIndex++] = region->v1;
		
	verticesBuffer[bufferIndex++] = x1;
	verticesBuffer[bufferIndex++] = y2;
	verticesBuffer[bufferIndex++] = region->u1;
	verticesBuffer[bufferIndex++] = region->v1;
		
	numSprites++;
}

void SpriteBatch::drawSprite(float x, float y, float width, float height, float angle, TextureRegion * region){
	float halfWidth = width / 2;
		float halfHeight = height / 2;
		
		float rad = angle * ((1 / 180.0f) * PI);
		float cos = (float) std::cos(rad);
		float sin = std::sin(rad);
		
		float x1 = -halfWidth * cos - (-halfHeight) * sin;
		float y1 = -halfWidth * sin + (-halfHeight) * cos;
		float x2 = halfWidth * cos - (-halfHeight) * sin;
		float y2 = halfWidth * sin + (-halfHeight) * cos;
		float x3 = halfWidth * cos - halfHeight * sin;
		float y3 = halfWidth * sin + halfHeight * cos;
		float x4 = -halfWidth * cos - halfHeight * sin;
		float y4 = -halfWidth * sin + halfHeight * cos;
		
		x1 += x;
		y1 += y;
		x2 += x;
		y2 += y;
		x3 += x;
		y3 += y;
		x4 += x;
		y4 += y;
		
		verticesBuffer[bufferIndex++] = x1;
		verticesBuffer[bufferIndex++] = y1;
		verticesBuffer[bufferIndex++] = region->u1;
		verticesBuffer[bufferIndex++] = region->v2;
		
		verticesBuffer[bufferIndex++] = x2;
		verticesBuffer[bufferIndex++] = y2;
		verticesBuffer[bufferIndex++] = region->u2;
		verticesBuffer[bufferIndex++] = region->v2;
		
		verticesBuffer[bufferIndex++] = x3;
		verticesBuffer[bufferIndex++] = y3;
		verticesBuffer[bufferIndex++] = region->u2;
		verticesBuffer[bufferIndex++] = region->v1;
		
		verticesBuffer[bufferIndex++] = x4;
		verticesBuffer[bufferIndex++] = y4;
		verticesBuffer[bufferIndex++] = region->u1;
		verticesBuffer[bufferIndex++] = region->v1;
		
		numSprites++;
}