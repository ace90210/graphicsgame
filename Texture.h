#pragma once

#include <windows.h>		// Header File For Windows
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include "SOIL.h"

#include <string>

class Texture
{
public:
	GLuint textureId;
	double width;
	double height;

	Texture(void);
	Texture(std::string texture);
	~Texture(void);
};

