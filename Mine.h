#pragma once
#include "DynamicGameObject.h"
#include "Nuke.h"
#include "FMODsound.h"
#include "CollisionGrid.h"


class Mine :
	public DynamicGameObject
{
public:
	static TextureRegion* mineRegion;

	CollisionGrid* grid;
	Nuke* explosion;
	bool alive;


	Mine (double x, double y, double width, double height, double rotation, CollisionGrid* grid);
	Mine(void);
	~Mine(void);


	
	void performCollision(GameObject* otherObject, float delta);

	TextureRegion* createMineRegion();


	
	void update(double delta);
	void draw();
};

