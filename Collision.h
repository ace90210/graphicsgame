#ifndef COLLISION_H
#define COLLISION_H

#include "Matrix.h"
#include "Shape.h"

int SAT2D(Shape *p1, Shape *p2);
int SATest(float *proj, float len);

#endif