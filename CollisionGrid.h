#pragma once
#include <vector>
#include "GameObject.h"

using namespace std;

class CollisionGrid
{
public:
	vector<GameObject*> objs; //objects to be tested for collision
	

	CollisionGrid(void);
	~CollisionGrid(void);

	void registerObject(GameObject* obj);
	void unregisterObject(GameObject* obj);

	int update(float delta);

	vector<int> getCollisionsWith(GameObject* obj);
};

