#include "DynamicGameObject.h"
#include <math.h> 
#include "Collision.h"

#define M_PI 3.14159265358979323846
#define MAX_TURN_SPEED 80

const double DynamicGameObject::TO_RADIANS = (1 / 180.0f) * M_PI;	
const float DynamicGameObject::TO_DEGREES = (1 / M_PI) * 180;

DynamicGameObject::DynamicGameObject()
{
}

DynamicGameObject::~DynamicGameObject()
{

}

DynamicGameObject::DynamicGameObject(double x = 0, double y = 0, double width = 1, double height = 1, double rotation = 0, TextureRegion* textureRegion = new TextureRegion(), int maxLife = 1, bool solid = true, double speed = 0)
	:  GameObject(x, y, width, height, rotation, textureRegion, maxLife, solid)
{
	this->velocity = Vector2(0, 0);
	this->rotationalVelocity = 0;
	this->speed = 0;
	this->maxSpeed = speed;
	distanceMovedLastFrame = 0;
	lastTurnValue = 0;
}

void DynamicGameObject::update(double delta)
{	
	//stop moving if within minimum speed threshold
	if(speed > STOP_THRESHOLD || speed < -STOP_THRESHOLD)
	{
 		Vector2 currentPosition = this->getPosition();
		double currentX = currentPosition.x;
		double currentY = currentPosition.y;
		long double distance = this->speed * delta;


		long double rad = this->getRotation() * TO_RADIANS;
		long double distanceX = sin(rad) * distance;
		long double distanceY = cos(rad) * distance;

		distanceMovedLastFrame = sqrt(distanceX * distanceX + distanceY * distanceY);

		this->addPosition(distanceX, distanceY);
	}
	else
	{
		distanceMovedLastFrame = 0;
	}
	if(this->rotationalVelocity != 0)
	{
		this->rotate(this->rotationalVelocity * delta);
		lastTurnValue = this->rotationalVelocity * delta;
	}
	else
	{
		lastTurnValue = 0;
	}
	
}

void DynamicGameObject::rotate(double angle)
{	
	GameObject::rotate(angle);
}

void DynamicGameObject::setRotation(double angle)
{	
	GameObject::setRotation(angle);
}

void DynamicGameObject::setSpeed(double speed)
{
	if( speed > maxSpeed)
	{
		this->speed = maxSpeed;
	}
	else if (speed < -maxSpeed)
	{
		this->speed = -maxSpeed;
	}
	else
	{
		this->speed = speed;
	}
}

void DynamicGameObject::increaseSpeed(double acceleration)
{
	if(speed < maxSpeed)
	{
		if(speed + acceleration > maxSpeed)
		{
			this->speed = maxSpeed;
		}
		else
		{
			this->speed += acceleration;
		}
	}	
}

void DynamicGameObject::decreaseSpeed(double deceleration)
{
	if(speed > -maxSpeed)
	{
		if(speed - deceleration < -maxSpeed)
		{
			this->speed = -maxSpeed;
		}
		else
		{
			this->speed -= deceleration;
		}
	}	
}

void DynamicGameObject::stop()
{
	this->speed = 0;
}

void DynamicGameObject::setTurn(double speed)
{
	if(speed > MAX_TURN_SPEED)
	{
		rotationalVelocity = MAX_TURN_SPEED;
	}
	else if(speed < -MAX_TURN_SPEED)
	{
		rotationalVelocity = -MAX_TURN_SPEED;
	}
	else
	{
		rotationalVelocity = speed;
	}
}

void DynamicGameObject::stopTurn()
{
	rotationalVelocity = 0;
}

void DynamicGameObject::performCollision(GameObject* otherObject, float delta)
{

	if(otherObject->collisionId != BULLET)
	 {	
		double lastTurn = lastTurnValue;
		
		
		this->rotate(-2 * lastTurn);
		this->stopTurn();

		if(speed >= STOP_THRESHOLD || speed <= -STOP_THRESHOLD)
		{
 			Vector2 currentPosition = this->getPosition();
			double currentX = currentPosition.x;
			double currentY = currentPosition.y;
			long double distance = -this->speed * 2 * delta; //reverse direction


			long double rad = this->getRotation() * TO_RADIANS;
			long double distanceX = sin(rad) * distance;
			long double distanceY = cos(rad) * distance;

			this->addPosition(distanceX, distanceY);
		}
		
	

		//if(!SAT2D(&this->boundingBox, &otherObject->boundingBox))
		//{				
 	//		Vector2 currentPosition = this->getPosition();
		//	double currentX = currentPosition.x;
		//	double currentY = currentPosition.y;
		//	long double distance = -this->speed * delta; //reverse direction


		//	long double rad = this->getRotation() * TO_RADIANS;
		//	long double distanceX = sin(rad) * distance;
		//	long double distanceY = cos(rad) * distance;

		//	this->addPosition(distanceX, distanceY);
		//	
		//}
		//
			
			speed = 0;
	}
}