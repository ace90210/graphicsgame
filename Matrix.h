#ifndef MATRIX_H
#define MATRIX_H

#include <stdio.h>
#include <math.h>
#include "Vector2.h"

void printMat(float *);
void setIdentity(float *);
void setRotMat(float *, float, int);
void setTraMat(float *, float, float, float);
void MultMat(float *,float *,float *);
void MultMatPostVec(float *M, float *vo, float *v);
void MultMatPreVec(float *M, float *vo, float *v);
void MultMatPre2DPoint(float *M, Vector2 *v, Vector2 *vN);

#endif