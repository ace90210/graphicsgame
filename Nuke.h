#pragma once
#include "FMODsound.h"
#include "AnimatedGameObject.h"
#include <vector>

class Nuke : public AnimatedGameObject
{
public:
	static Texture* atlas;
	static SoundEffect* explosion;

	bool loaded, play, done;
	double explosionLife;
	static std::vector<TextureRegion>* regions;
	int frameNum;

	Nuke(void);
	Nuke(double x, double y, double width, double height, double duration);
	~Nuke(void);

	std::vector<TextureRegion> createRegions();

	virtual void update(double delta);

	void performCollision(GameObject* otherObject, float delta);

	void startPlayback(float volume);

	void unload();
};

