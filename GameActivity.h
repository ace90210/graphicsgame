//
// The structure of the Graphics 1 OpenGL template is explained in README.txt
//

// Ensure that this file can only be included once
#pragma once

#include "Activity.h"

#include "AnimatedTexture.h"
#include "AnimatedPlayer.h"
#include "Tank.h"
#include "DynamicGameObject.h"
#include "CollisionGrid.h"
#include "Background.h"
#include "NPCTank.h"
#include "InvisibleWall.h"
#include "Wall.h"
#include "TankTrap.h"
#include "Mine.h"

#include "TileEntity.h"
#include "Nuke.h"

// GAME ACTIVITY
// Renders a grid with a smiley face character.
// WASD keys move the smiley face
// ARROW keys move the camera/viewpoint

#define VIEW_SIZE 30.0					// The height of the view in WORLD SPACE
#define BASE_SCALE 20

const int mapW = 10, mapH = 10;

class GameActivity : public Activity
{
private:
	// GAME activity variables go here

	static SoundEffect musicNormal;
	static SoundEffect musicThreat;

	InvisibleWall leftWall;
	InvisibleWall rightWall;
	InvisibleWall topWall;
	InvisibleWall bottomWall;

	//Mine testMine;

	// Player position
	GLuint playerTextureID;
	double time, mouseX, mouseY, TankX, distanceX, fps;
	Vector2 tankResetPosition;

	//GameObject player;
	//AnimatedPlayer* animation;
	
	Tank* tank;
	std::vector<NPCTank*> enemies;
	std::vector<TankTrap*> testTraps;
	std::vector<Mine*> testMines;
	
	std::vector<GameObject*> layer0Objects;
	std::vector<GameObject*> layer1Objects;
	std::vector<GameObject*> layer2Objects;
	std::vector<GameObject*> layer3Objects;

	std::vector<DynamicGameObject*> dynamicTileObjects;

	std::vector<GameObject*> guiItems;
	
	vector<Background*> backgrounds;
	Background* background;
	 
	bool camFollow, playerWins, nextWave, nextLevel, healthText;
	int hits, inventoryTraps, inventoryMines, shotsFired, totalEnemyHealth;

	
	
	

	static CollisionGrid grid;

public:
	static int level;
	static int wave;
	static unsigned int score;
	//static Nuke testNuke;
	//static int map[5][5];
	static TileEntity map[mapW][mapH];
	static font_data hp_font;
	// Camera position
	static double camX, camY, screenWidth, screenHeight, timeSinceThreatPlayed;
	static double aspectRatio;
	static int seenByTanks;
	static std::vector<Nuke*> explosions;


	GameActivity(OpenGLApplication *app);

	static void addToScore(unsigned int amount);

	static void addExplosion(double x, double y);

	static void startThreatMusic();
	static void stopThreatMusic();
	
	static void addSeenByTank();



	/*
	*
	* GAME activity methods
	* You should put your game code in the implementations of these methods
	* See GameActivity.cpp
	*
	*/
	virtual void initialise();											// Called on application start up
	virtual void shutdown();											// Called on application shut down

	virtual void onSwitchIn();											// Activity switch in; called when the activity changes and this one switches in
	virtual void onReshape(int width, int height);						// called when the window is resized
	virtual void update(double deltaT, double prevDeltaT);				// Update the application; if the current frame is frame number F, then the previous frame is F-1 and the one before that is F-2
																		// deltaT is the time elapsed from frame F-1 to frame F, prevDeltaT is the time elapsed from F-2 to F-1
	virtual void render();												// Render function


	virtual void onMouseDown(int button, int mouseX, int mouseY);		// Called when mouse button pressed
	virtual void onMouseUp(int button, int mouseX, int mouseY);			// Called when mouse button released
	virtual void onMouseMove(int mouseX, int mouseY);					// Called when mouse moved
	virtual void onKeyDown(int key);									// Called when key pressed
	virtual void onKeyUp(int key);										// Called when key released

	void makeTileMap();

	void loadTileMap(int level, int wave);

	void genTileMap(float left, float bottom, float width, float height);
	void registerStaticEntity(GameObject* entity, int layer);
	void registerDynamicEntity(DynamicGameObject* entity, int layer);

	void resetMap();

	void generateGui();
	void registerGuiItem(GameObject* guiItem);

	// Drawing a grid in the background; useful for debugging
	void renderDebugGrid(float left, float bottom, float width, float height, int hSegments, int vSegments);

	void updateExplosions(double delta);
};