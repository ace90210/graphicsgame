//
// The structure of the Graphics 1 OpenGL template is explained in README.txt
//
/*
Start screen activity implementation
*/

#include <windows.h>		// Header File For Windows
#include <gl/gl.h>			// Header File For The OpenGL32 Library
#include <gl/glu.h>			// Header File For The GLu32 Library

#include "SOIL.h"

#include "OpenGLApplication.h"			// Needed for OpenGLApplication method calls
#include "LoadingScreenActivity.h"


#include "freetype.h"		// Header for font library.
using namespace freetype;


font_data loading_font;

LoadingScreenActivity::LoadingScreenActivity(OpenGLApplication *app)
	: Activity(app)		// Call the super constructor
{

	this->progress = 0;
	this->firstRun = true;
	loading_font.init("arialbd.TTF", 36);					    //Build the freetype font
	
	loadAssets.push_back(app->startScreen);	
	loadAssets.push_back(app->game);
	loadAssets.push_back(app->splashScreen);
	loadAssets.push_back(app->endScreen);
	loadAssets.push_back(app->winScreen);
}


void LoadingScreenActivity::initialise()
{
	// Initialise the activity; called at application start up

	// Load the start screen image as a texture using the SOIL library
	textureID = SOIL_load_OGL_texture("loading_screen.png",		// filename
		SOIL_LOAD_AUTO,											// 
		SOIL_CREATE_NEW_ID,										// ask SOIL to create a new OpenGL texture ID for us
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);				// generate Mipmaps and invert Y
}


void LoadingScreenActivity::shutdown()
{
	// Shutdown the activity; called at application finish

	// Delete the texture
	glDeleteTextures(1, &textureID);
}


/*
*
* ACTIVITY METHODS
* Put your application/game code here
*
*/
void LoadingScreenActivity::onSwitchIn()
{
	// Activity switched in

		glClearColor(0.37f, 0.49f, 0.32f, 0.0f);						//sets the clear colour to same as loading image background
}

void LoadingScreenActivity::onReshape(int width, int height)
{
	// Screen resized
	glViewport(0,0,width,height);						// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	double aspect = app->getAspectRatio();
	gluOrtho2D(-aspect, aspect, -1.0, 1.0);				// Set a projection that takes the area -aspect to aspect in X and -1 to 1 in Y and project it to -1 to 1 in both X and Y

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
}
void LoadingScreenActivity::update(double deltaT, double prevDeltaT)
{
	//avoid loading before displaying screen
	if(!this->firstRun)
	{
		if(progress < this->loadAssets.size())
		{
			loadAssets[progress]->initialise();
			progress++;
		}
		else
		{
			app->setCurrentActivity(app->splashScreen);
		}
	}
	else
	{
		this->firstRun = false; //ran once now set to first run
	}
}


void LoadingScreenActivity::render()
{
	// OpenGL render calls go in this method

	// Clear color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	// Identity matrix
	glLoadIdentity();

	// Bind our start screen texture to GL_TEXTURE_2D
	glBindTexture(GL_TEXTURE_2D, textureID);
	// Enable 2D texturing
	glEnable(GL_TEXTURE_2D);

	// Use two triangles to make a square, with texture co-ordinates for each vertex
	glBegin(GL_TRIANGLES);
		glTexCoord2f(0, 0);
		glVertex2f(-1, -1);

		glTexCoord2f(1, 0);
		glVertex2f(1, -1);

		glTexCoord2f(0, 1);
		glVertex2f(-1, 1);


		glTexCoord2f(1, 0);
		glVertex2f(1, -1);

		glTexCoord2f(1, 1);
		glVertex2f(1, 1);

		glTexCoord2f(0, 1);
		glVertex2f(-1, 1);
	glEnd();
	

	double percent =  ((double)this->progress / (double)this->loadAssets.size()) * 100;
	//now draw loading bar background	
	glPushMatrix();
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
		glBegin(GL_QUADS);

			//draw all red bar, then overlay health
			glColor4f(0, 1, 0, 1);

			glVertex2f(-0.8, -0.4);
			glVertex2f(-0.8, -0.3);
			glVertex2f(0.8, -0.3);
			glVertex2f( 0.8, -0.4);

			glColor4f(1, 1, 1, 1);
		glEnd();
				
	glPopMatrix();

	
	//calc width of health bar
	double w =  2  * ((double)this->progress / (double)this->loadAssets.size());

	glPushMatrix();
		glBegin(GL_QUADS);			
			glColor4f(1, 0, 0, 1);

			glVertex2f(-0.8 + w, -0.4);
			glVertex2f(-0.8 + w, -0.3);
			glVertex2f(0.8, -0.3);
			glVertex2f( 0.8, -0.4);

			glColor4f(1, 1, 1, 1);
		glEnd();
				
	glPopMatrix();





	glLoadIdentity();	
	glColor3f(0.90f,  0.90f, 0.95f);
	
	print(loading_font, this->app->getScreenWidth() / 2, this->app->getScreenHeight() / 2 , "%3.2f%%", percent);
	glColor3f(1,  1, 1);

	glFlush();
}



void LoadingScreenActivity::onKeyUp(int key)										// Called when key released
{
	// Key released

	// Exit the start screen when the SPACE key is released, NOT pressed
	// That way the next activity starts with the space key NOT pressed
	
}
