#pragma once
#include "GameObject.h"
#include "Vector2.h"
#include "CollisionGrid.h"
#include "TextureRegion.h"

class InvisibleWall: public GameObject
{
public:
	static TextureRegion* wallRegion;

	InvisibleWall(void);
	InvisibleWall(Vector2 start, double width, double height, double rotation);
	~InvisibleWall(void);

	TextureRegion* createWallRegion();

	void performCollision(GameObject* otherObject, float delta);
};

