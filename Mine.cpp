#include "Mine.h"
#include "GameActivity.h"

TextureRegion* Mine::mineRegion = NULL;


#define SOUND_RANGE 35
#define MIN_SOUND 0.2

Mine::Mine(void)
{
}


Mine::~Mine(void)
{
}

Mine::Mine (double x, double y, double width, double height, double rotation, CollisionGrid* grid)
	: DynamicGameObject( x, y, width, height, rotation, createMineRegion(), 1, true, 0)
{
	this->grid = grid;
	collisionId = MINE; 
	collidesWith = TANK | MINE | EXPLOSION; //only collides with tanks or bullets

	this->setBoundingBox(14, 12, 37, 37); //co-ords from original image
	
	
	explosion = NULL;
}




void Mine::update(double delta)
{

	if(currentLife < 1 && alive)
	{	
		alive = false;
		GameActivity::addExplosion(this->getPosition().x, this->getPosition().y);
		//if(explosion != NULL)
		//{
		//	if(explosion->done)
		//	{
		//		//done exploding delete this bullet
		//		grid->unregisterObject(this);
		//		explosion->unload();
		//		alive = false;
		//		return;
		//	}
		//	explosion->update(delta);
		//}
		//else
		//{
		//	explosion = new Nuke(this->getPosition().x, this->getPosition().y, 3, 3, 0.016667);
		//	double camDistance = Vector2::distance(this->getPosition(), Vector2(GameActivity::camX, GameActivity::camY));
		//	double vol = 1 - (camDistance / SOUND_RANGE);
		//	if(vol < MIN_SOUND)
		//	{
		//		vol = MIN_SOUND;
		//	}
		//	explosion->startPlayback(vol);
		//}
	}
	
}


void Mine::performCollision(GameObject* otherObject, float delta)
{
	currentLife--;
	//otherObject->currentLife--;
}


TextureRegion* Mine::createMineRegion()
{
	if(mineRegion == NULL)
	{
		mineRegion = new TextureRegion("mine.png");
	}
	return mineRegion;
}


void Mine::draw()
{
	if(explosion == NULL)
	{
		DynamicGameObject::draw();
	}
	else
	{
		explosion->draw();
	}
}